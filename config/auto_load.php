<?php session_start();

include('koneksi.php');

// $base_url = "http://localhost:809/pwpm/musywil";
$domain = $_SERVER['HTTP_HOST'];
$domain .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
$base_url = "http://" . $domain;
if (!empty($_SERVER['HTTPS'])) {
	$base_url = "https://" . $domain;
}


$uri_segment = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$ius=count($uri_segment)-2;//indek_level_uri_segment
// var_dump($uri_segment);

if(isset($_SESSION['status']) && $_SESSION['status'] == 'login') {
    // lanjut
    if(level_base_url() == $_SESSION['level']) {
        // lanjut
    } else {
        echo "Error: Forbidden !!!";
        echo "<br><br> <button type='button' onclick='history.back()'> Kembali </button>";
        die;
    }

} else {
    $_SESSION['login_error'] = "Silahkan Login untuk masuk kedalam sistem";
    
    header('location:'.home_base_url().'login');
}
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);
// function base_url($url){
//     GLOBAL $base_url;
//     return $base_url.'/'.$url;
// }
function _pwd($password='pmberkemajuan'){
    $salt='yd2smg';
    $p=password_hash($password.$salt, PASSWORD_BCRYPT,['cost' => 9]);
    return $p;
}
function home_base_url(){
    GLOBAL $base_url;
    return str_replace(explode("/",str_replace("http://","",str_replace("https://","",$base_url)))[(count(explode("/",str_replace("http://","",str_replace("https://","",$base_url))))-2)]."/","",$base_url);
}
function level_base_url(){
    GLOBAL $base_url;
    return str_replace('/','',str_replace(home_base_url(),"",$base_url));
}
function is_jpeg(&$pict)
  {
    return (bin2hex($pict[0]) == 'ff' && bin2hex($pict[1]) == 'd8');
  }

function is_png(&$pict)
  {
    return (bin2hex($pict[0]) == '89' && $pict[1] == 'P' && $pict[2] == 'N' && $pict[3] == 'G');
  }
function is_image($path)
{
    $a = getimagesize($path);
    $image_type = $a[2];

    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    {
        return true;
    }
    return false;
}
function base_url($url){
    GLOBAL $base_url;
    return $base_url.$url;
}
function rupiah($angka)
{

	$hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
	return $hasil_rupiah;
}
function count_utusan($kode){
    $sql="select count(*) n from pendaftar where hapus='0' and user_daerah='".$kode."';";
    return _sq($sql)->fetch_object()->n;
}
function count_utusan_verifikasi($kode){
    $sql="select count(*) n from pendaftar a left join tbl_verifikasi_pendaftar b on a.id=b.id_pendaftar 
    left join data_kirim c on a.user_daerah=c.daerah where a.user_daerah='".$kode."' and a.hapus='0' and b.verifikasi='1' and c.id is not null;";
    return (_sq($sql)->fetch_object()->n=''?'0':_sq($sql)->fetch_object()->n);
}
function user_nama($id){
    $sql="select nama from users where id='".$id."';";
    return _sq($sql)->fetch_object()->nama;
}
function _user_id(){
    return $_SESSION['id_users'];
}
function _user_nama(){
    return $_SESSION['nama'];
}
function _user_daerah(){
    return $_SESSION['daerah'];
}
function sesuaikan_tgl($tgl){//"27/10/1991"
    $date=explode("/",$tgl);
    $out=$date[2].'-'.$date[1].'-'.$date[0];
    return $out;
}
function id_data_kirim_to_kode_daerah($id_daerah){
    $out=query_row('select daerah from data_kirim where id="'.$id_daerah.'"','daerah');
    return $out;
}
function kode_daerah_to_id_data_kirim($kode_daerah){
    $out=query_row('select id from data_kirim where daerah="'.$kode_daerah.'"','id');
    return $out;
}
function nbm_to_id($nbm){
    $out=query_row('select id from pendaftar where nbm="'.$nbm.'"','id');
    return $out;
}
function nama_daerah($kode_daerah){
    if(strlen($kode_daerah)!=4){$out='-';}
    else{$out=query_row('select nama_daerah from tbl_daerah where kode_daerah="'.$kode_daerah.'"','nama_daerah');}
    return $out;
}
function no_daerah($kode_daerah){
    if(strlen($kode_daerah)!=4){$out='-';}
    else{$out=query_row('select no_daerah from tbl_daerah where kode_daerah="'.$kode_daerah.'"','no_daerah');}
    return $out;
}
function nama_cabang($kode_cabang){
    if(strlen($kode_cabang)!=6){$out='-';}
    else{$out=query_row('select nama_cabang from tbl_cabang where kode_cabang="'.$kode_cabang.'"','nama_cabang');}
    return $out;
}
function query_row($sql,$field_select){
    GLOBAL $koneksi;
    
    $result = mysqli_query($koneksi,$sql);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    return $row[$field_select]; 
}
function modal($id,$title,$html,$add='',$tombol='<button class="btn btn-secondary" type="button" data-dismiss="modal">Exit</button>'){
    return '
    <div class="modal fade  " id="'.$id.'" tabindex="-1"role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" '.$add.'  >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">'.$title.'</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">'.$html.'</div>
                <div class="modal-footer">
                    '.$tombol.'
                </div>
            </div>
        </div>
    </div>';
}
function callApiWithURLBearerToken($url, $data, $token = '')
{
	$data = json_encode($data, JSON_UNESCAPED_SLASHES);
	//echo $data;
	$token = token();
	$authorization = "Authorization: Bearer " . $token;
	if ($token != '') {
		$process = curl_init();
		curl_setopt($process, CURLOPT_URL, $url);
		curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($process, CURLOPT_POSTFIELDS, $data);

		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
	}
	return $return;
}
 function lur_notif_wa_sikenal()
{
	#return 'https://graph.facebook.com/v15.0/105326405789558/messages';
// return 'https://graph.facebook.com/v17.0/105326405789558/messages';
 return 'https://graph.facebook.com/v17.0/106694218996764/messages';
}
function kirim_notif_wa($number = '6285726922293', $msg = 'Terima kasih')
{
	//db_insert('wa_keluar', array('to' => $number, 'message' => $msg, 'time' => date('Y-m-d H:i:s')));
	$data = callApiWithURLBearerToken(lur_notif_wa_sikenal(), template_notif_wa($number, $msg), token());
	return $data;
}
function template_notif_wa($number = '6285726922293', $msg = 'Terima kasih')
{
	$data = [
		"messaging_product" => "whatsapp",
		"recipient_type" => "individual",
		"to" => $number,
		"type" => "template",
		"template" => [
			"name" => "musywil_pwpm_jateng",
			"language" => [
				"code" => "id",
				"policy" => "deterministic"
			],
			"components" => array(
				[
					"type" => "body",
					"parameters" => array(
						[
							"type" => "text",
							"text" => $msg
						]
					)
				]
			)
		]
	];
	return $data;
}
function token()
{
    $token='EABTFZCEZC9ygoBAOpWMimOTisZCD7pT16c6pHzWgBhR2Q24V98TSfMNZAZAIgO1ZBTZAuCZBKCX44o6FBIu8oW8eeXZCqTs3Eyqc3sjlTHE7aXqSq01afR6gvDhk1njrvX8HZCEgnQX5Y7whtyVO5OxLdt533v49nDA5I45P20ZAMhnZC9H1ZCNVoLaNOEGv435ZCOqZCpjXXppW4usWAZDZD';
	return $token;
}
function bulan($rrr)
	{
		if($rrr=='1' || $rrr=='01'){$ttt='Januari';}
		if($rrr=='2' || $rrr=='02'){$ttt='Februari';}
		if($rrr=='3' || $rrr=='03'){$ttt='Maret';}
		if($rrr=='4' || $rrr=='04'){$ttt='April';}
		if($rrr=='5' || $rrr=='05'){$ttt='Mei';}
		if($rrr=='6' || $rrr=='06'){$ttt='Juni';}
		if($rrr=='7' || $rrr=='07'){$ttt='Juli';}
		if($rrr=='8' || $rrr=='08'){$ttt='Agustus';}
		if($rrr=='9' || $rrr=='09'){$ttt='September';}
		if($rrr=='10'){$ttt='Oktober';}
		if($rrr=='11'){$ttt='November';}
		if($rrr=='12'){$ttt='Desember';}
		return $ttt;
	}
	function bulan_singkat($rrr)
	{
		if($rrr=='1' || $rrr=='01'){$ttt='Jan';}
		if($rrr=='2' || $rrr=='02'){$ttt='Feb';}
		if($rrr=='3' || $rrr=='03'){$ttt='Mar';}
		if($rrr=='4' || $rrr=='04'){$ttt='Apr';}
		if($rrr=='5' || $rrr=='05'){$ttt='Mei';}
		if($rrr=='6' || $rrr=='06'){$ttt='Jun';}
		if($rrr=='7' || $rrr=='07'){$ttt='Jul';}
		if($rrr=='8' || $rrr=='08'){$ttt='Ags';}
		if($rrr=='9' || $rrr=='09'){$ttt='Sep';}
		if($rrr=='10'){$ttt='Okt';}
		if($rrr=='11'){$ttt='Nop';}
		if($rrr=='12'){$ttt='Des';}
		return $ttt;
	}

	function hari_now()
		{
		$input=date('D');
		if($input=='Sun'){$output='Minggu';}
		if($input=='Mon'){$output='Senin';}
		if($input=='Tue'){$output='Selasa';}
		if($input=='Wed'){$output='Rabu';}
		if($input=='Thu'){$output='Kamis';}
		if($input=='Fri'){$output='Jumat';}
		if($input=='Sat'){$output='Sabtu';}
		return $output;
		}
	function hari($in)
		{
		$tgl = substr($in,8,2);
		$bln = substr($in,5,2);
		$thn = substr($in,0,4);
		$stamp = mktime(0,0,0,$bln,$tgl,$thn);
		$input= date('D',$stamp);
		if($input=='Sun'){$output='Minggu';}
		if($input=='Mon'){$output='Senin';}
		if($input=='Tue'){$output='Selasa';}
		if($input=='Wed'){$output='Rabu';}
		if($input=='Thu'){$output='Kamis';}
		if($input=='Fri'){$output='Jumat';}
		if($input=='Sat'){$output='Sabtu';}
		return $output;
		}
	function tanggal_lengkap()
		{
		$output=hari_now()." ".date('j')."-".bulan(date('m'))."-".date('Y');
		return $output;
		}
	function tanggal_resmi()
		{
		$output=date('d')." ".bulan(date('m'))." ".date('Y');
		return $output;
		}
	function timestamp_to_tanggal($timestamp)
		{
		$tgl = date('d',$timestamp);
		$bln = date('n',$timestamp);
		$thn = date('Y',$timestamp);
		$output=$tgl." ".bulan($bln)." ".$thn;
		return $output;
		}
	function timestamp_to_tanggal2($timestamp)
		{
		$tgl = date('d',$timestamp);
		$bln = date('n',$timestamp);
		$thn = date('Y',$timestamp);
		$output=$tgl." ".bulan_singkat($bln)." ".$thn;
		return $output;
		}
	function date_to_tanggal($in)        #2011-12-17
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			if(checkdate($bln,$tgl,$thn))
			{
			   $out=substr($in,8,2)." ".bulan_singkat(substr($in,5,2))." ".substr($in,0,4);
			}
			else
			{
			   $out = "-error-";
			}
			return $out;
		}
	function timeline_date_to_tanggal($in)        #2011-12-17
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			if(checkdate($bln,$tgl,$thn))
			{
			   $out=substr($in,8,2)." ".bulan_singkat(substr($in,5,2)).", ".substr($in,0,4);
			}
			else
			{
			   $out = "-error-";
			}
			return $out;
		}
	function date_to_tanggal2($in)        #2011-12-17
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			if(checkdate($bln,$tgl,$thn))
			{
			   $out=substr($in,8,2)." ".bulan(substr($in,5,2))." ".substr($in,0,4);
			}
			else
			{
			   $out = "-error-";
			}
			return $out;
		}
	function datetime_to_tanggal_custom($in,$error='error')#2011-05-22 03:25:23
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			$jam = substr($in,11,8);
			
			if(checkdate($bln,$tgl,$thn))
			{
			   $out=substr($in,8,2)." ".bulan_singkat(substr($in,5,2))." ".substr($in,0,4)." ".$jam."";
			}
			else
			{
			   $out = $error;
			}
			return $out;
		}
	function datetime_to_tanggal_custom2($in,$error='error')#2011-05-22 03:25:23
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			$jam = substr($in,11,8);
			
			if(checkdate(intval($bln),intval($tgl),intval($thn)))
			{
			   $out=substr($in,8,2)." ".bulan(substr($in,5,2))." ".substr($in,0,4)." ".$jam."";
			}
			else
			{
			   $out = $error;
			}
			return $out;
		}
	function datetime_to_tanggal_custom2_wa($in,$error='error')#2011-05-22 03:25:23
		{
			$tgl = substr($in,8,2);
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			$jam = substr($in,11,8);
			
			if(checkdate($bln,$tgl,$thn))
			{
			   $out=substr($in,8,2)." ".bulan(substr($in,5,2))." ".substr($in,0,4)." Pukul ".substr(str_replace(':','.',$jam),0,5)."";
			}
			else
			{
			   $out = $error;
			}
			return $out;
		}

function _row($tabel,$select,$field,$where=''){
    GLOBAL $koneksi;
    if($where==''){return 0;}
    else{
        return _sq('select '.$select.' from '.$tabel.' where '.$where)->fetch_object()->$field;
        // return 'select '.$select.' from '.$tabel.' where '.$where;
    }
}
function _jumlah_pcpm($kode_daerah){
    GLOBAL $koneksi;
    
        return _sq('select count(*) n from (SELECT count(*) as no FROM `pendaftar` where kode_pimpinan=3 and user_daerah="'.$kode_daerah.'" GROUP by kode_pimpinan,pimpinan) y')->fetch_object()->n;

}
function kirim_data($field,$value){
    GLOBAL $koneksi;
    $sql_insert = "insert into data_kirim ($field) value ('".implode("','",$value)."');";
    if(_sq($sql_insert)){
        return 1;
    }
    else{
        return '0';
    }
}
function catatan($action,$log){
    GLOBAL $koneksi;
    $sql_insert = 'insert into catatan (action,catatan,user_id,time) value ("'.$action.'","'.$log.'","'._user_id().'","'.date('Y-m-d').'");';
    if(_sq($sql_insert)){
            return 1;
    }
    else{
            return 0;
    }
}
function import($field,$value,$f,$v){
    GLOBAL $koneksi;
    $sql_insert = "insert into pendaftar ($field) value (".implode(",",$value).");";
    $nbm=$value[7];//nbm kolom ke 7
    $sql_user = "SELECT nbm FROM pendaftar where nbm = $nbm";
    $sql_update="UPDATE pendaftar set ";
    for($x=0;$x<count($f);$x++){
        if($x==(count($f)-1)){
            $sql_update.=$f[$x]."='".addslashes($v[$x])."' ";
        }else{
            $sql_update.=$f[$x]."='".addslashes($v[$x])."', ";
        }
    }
    $sql_update.="where nbm = $nbm ;";


    // $result_user = mysqli_query($koneksi, $sql_user);
    $result_user = _sq($sql_user);

    if(mysqli_num_rows($result_user)> 0) {
        // if(mysqli_query($koneksi, $sql_update)){
        if(_sq($sql_update)){
            return '2';
        }
        else{
            return '3';
        }
    }
    else{
        // if(mysqli_query($koneksi, $sql_insert)){
        if(_sq($sql_insert)){
            return '1';
        }
        else{
            return '4';
        }
    }
    // return $sql_update;
    // return implode('+',$value).'# #'.$sql_insert.'<br/>'.$sql_update.'<hr/>';//$sql_user.mysqli_num_rows($result_user);
}

function desc_kodepim($kode){
		if($kode=='1'){ return 'PWPM';}
		else if($kode=='2'){ return 'PDPM';}
		else{ return 'PCPM';}
	}
function _smgenc($text, $kata_dasar = 'NETNUSANTARA-YD2SMG'){
		$text_encrypted = _md5_encrypt($text,$kata_dasar.'yd2smg');
		return $text_encrypted;
	}
function _smgdec($text, $kata_dasar = 'NETNUSANTARA-YD2SMG'){
		$text_decrypted = _md5_decrypt($text,$kata_dasar.'yd2smg');
		return $text_decrypted;
	}
function _md5_encrypt($plain_text, $password, $iv_len = 16)
	{
		$plain_text .= "\x13";
		$n = strlen($plain_text);
		if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
		$i = 0;
		$enc_text = _get_rnd_iv($iv_len);
		$iv = substr($password ^ $enc_text, 0, 512);
		while ($i < $n) {
		$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
		$enc_text .= $block;
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
		}
		#$hasil=_base64_encode($enc_text);
		$hasil=base64_encode($enc_text);
		$hasil = str_replace('+', '@', $hasil);
		$hasil = str_replace('/', '_', $hasil);
		$hasil = str_replace('=', 'yd2smg', $hasil);
		return $hasil.'.html';
	}

	function _md5_decrypt($enc_text, $password, $iv_len = 16)
	{
		$enc_text = str_replace('.html', '', $enc_text);
		$enc_text = str_replace('@', '+', $enc_text);
		$enc_text = str_replace('_', '/', $enc_text);
		$enc_text = str_replace('yd2smg', '=', $enc_text);
		#$enc_text = _base64_decode($enc_text);
		$enc_text = base64_decode($enc_text);
		$n = strlen($enc_text);
		$i = $iv_len;
		$plain_text = '';
		$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
		while ($i < $n) {
			$block = substr($enc_text, $i, 16);
			$plain_text .= $block ^ pack('H*', md5($iv));
			$iv = substr($block . $iv, 0, 512) ^ $password;
			$i += 16;
		}
		return preg_replace('/\\x13\\x00*$/', '', $plain_text);
	}
	function _get_rnd_iv($iv_len)
	{
		$iv = '';
		while ($iv_len-- > 0) {
			$iv .= chr(mt_rand() & 0xff);
		}
		return $iv;
	}

?>