<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Pendaftar</h1>
                    <div class="row">
                    <div class="col-md-6">                                
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-danger">Kewajiban Pembayaran</h6>
                                </div>
                                <div class="card-body">
                                    <p class="text-primary">
                                        Peserta diwajibkan membayar registrasi berupa Sumbangan Wajib Organisasi (SWO)
                                        dan Sumbangan Wajib Peserta (SWP) dengan nominal yang telah ditentukan yaitu:
                                        <br>
                                        <br> SWO : Rp. 500.000,00-
                                        <br> SWP : Rp. 250.000,00-
                                        <br>
                                        <br> Pembayaran Melalui Rekening Bank BSI Atas Nama PM Jateng (005)5043332222
                                        <br> Setelah melakukan pembayaran, upload bukti di form di bawah ini.
                                    </p>
                                    <p class="text-danger">
                                        Hati-hati! Panitia hanya menyediakan 1 Rekening pembayaran, jika ada rekening lain 
                                        yang mengatasnamakan panitia musywil PM jateng, harap waspada penipuan! 
                                    </p>
                                </div>
                                <div class="p-5">
                                    <form action="" class="user" method="post" enctype="multipart/form-data">
                                        <label for="" class="form-group text-gray-900">Sumbangan Wajib Organisasi (SWO)</label>
                                        <input type="text" name="swo" class="form-control text-gray-900" readonly placeholder="Rp. 500.000,00">
                                        <input type="file" name="upload-swo" id="upload-swo" class="form-control">

                                        <label for="" class="form-group text-gray-900 mt-4">Sumbangan Wajib Peserta (SWP)</label>
                                        <input type="text" name="swp" class="form-control text-gray-900" readonly placeholder="Rp. 250.000,00">
                                        <input type="file" name="upload-swo" id="upload-swo" class="form-control">

                                        <div class="text-right"> 
                                            <a href="" class="btn btn-primary mt-3">
                                                UPLOAD
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            </div>
                        




                        </div>

                        

                        
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>