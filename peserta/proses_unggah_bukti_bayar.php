<?php 
include('../config/auto_load.php');
// include('../config/koneksi.php');
require '../assets/vendor/autoload.php';

if(!isset($_FILES['filenya'])){
    echo '<div class="col-md-12">
            <div class="alert alert-danger">
                <font color="red">Anda Belum pilih File!</font>
            </div>
        </div>';
    ?>
<div class="col-md-12">
    <div class="card md-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Unggah Bukti Bayar (JPG/PNG)</h6>
        </div>
        <div class="card-body">
                                            <form class="user" enctype="multipart/form-data">
                                                
                                                <div class="form-group">
                                                    <label class="small mb-1">Keterangan</label>
                                                    <input type="input" name="keterangan" class="form-control keterangan" placeholder="Keterangan File"/><br>
                                                    <label class="small mb-1">Unggah Foto Bukti Bayar</label><br>
                                                    <input type="file" name="filenya" class="file form-control" placeholder="Unggah Bukti Bayar"/>
                                                </div>
                                            
                                                <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Unggah</a>
                                            </form>
        </div>
    </div>
</div>
<div style="margin:5px;"></div>
                        <div class="col-md-12">
                            <!-- Import -->
                            <div class="card md-4">
                                <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Bukti Bayar</h6>
                                </div>
                                <div class="card-body table-responsive">
                                    <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No</td>
                                            <td>Keterangan</td>
                                            <td>Tanggal Unggah</td>
                                            <td>File Bukti Bayar</td>
                                            <td>Status Validasi</td>
                                            <td>Tanggal Validasi</td>
                                        </tr>
                                <?php
                                $sql_user = "SELECT * FROM tbl_bukti_bayar where daerah='"._user_daerah()."' order by id desc";
                                $result_user = mysqli_query($koneksi, $sql_user);
                                if(mysqli_num_rows($result_user)> 0) {
                                    $no=1;
                                    while($data_user = mysqli_fetch_array($result_user)){
                                        echo "<tr>
                                        <td>".$no.".</td>
                                        <td>".$data_user['keterangan_file']."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_unggah'])."</td>
                                        <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#buktibayar".$data_user['id']."\">Lihat Foto</a></td>
                                        <td>".$data_user['is_valid']."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_validasi'],'-')."</td>
                                        </tr>";
                                        //Modal Foto
                                        $html_foto="<image src='../".$data_user['path']."/".$data_user['file_name']."' width='450'><br/>".$data_user['nama'];
                                        echo modal('buktibayar'.$data_user['id'],'Bukti Bayar',$html_foto);
                                        
                                        
                                        $no++;
                                    }
                                }
                                else{
                                ?>
                                        <tr>
                                            <td colspan="9">- empty -</td>
                                        </tr>
                                <?php
                                }
                                ?>
                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')['0'];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    form_data.append('keterangan', $('.keterangan').val());
                    $.ajax({
                        url: '<?=base_url("proses_unggah_bukti_bayar.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>
    <?php 
    die();
} //end jika file kosong

    $date_data=date('Y-m-d H:i:s');
    // Config File upload lampiran F
    $rename_dokumen='Utusan_';
    $fileName = $_FILES['filenya']['name'];
    $tmpName  = $_FILES['filenya']['tmp_name'];
    $fileSize = $_FILES['filenya']['size'];
    $fileType = $_FILES['filenya']['type'];
    
    $config['upload_path']          = '../assets/uploads/bukti_bayar/';
    $target_file = $config['upload_path']   . basename($fileName);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"){ 
        echo '<div class="col-md-12">
            <div class="alert alert-danger">
                <font color="red">File "'.$fileName.'" Tidak diijinkan! <br/>File yang diijinkan (JPG/PNG).</font>
            </div>
        </div>';
}
else{
    // $spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($tmpName);
    // $reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    // $reader->setReadDataOnly(true);
    // $spreadsheet = $reader->load($tmpName);    
    
    // $rename_dokumen='Utusan_'.$spreadsheet->getActiveSheet()->getCell('B1')->getValue().'_';
    $rename_dokumen='Utusan_'._user_id().'_';
    
    $config['allowed_types']        = '*';
    $ext=explode('.',$fileName)[(count(explode('.',$fileName))-1)];
    $config['file_name']            = $rename_dokumen.time().".".$ext;
    $config['overwrite']			= true;
    $config['max_size']             = 2*MB;//2000; // 1MB
    
    if ($fileSize > $config['max_size'] ) {
        echo '<div class="col-md-12">
                <div class="alert alert-danger">
                    <font color="red">File melebihi batas! max 2MB File Anda '.round(($fileSize/MB),4).' MB</font>
                </div>
            </div>';
    }
    // else 
    // if($spreadsheet->getActiveSheet()->getCell('A3')->getValue()!='Nama Lengkap'){ 
    //     echo '<div class="col-md-12">
    //         <div class="alert alert-danger">
    //             <font color="red">Template Salah! Silahkan hubungi Admin</font>
    //         </div>
    //     </div>';
    // }
    else{
        if(!move_uploaded_file($tmpName, $config['upload_path'].$config["file_name"])){
            echo '<div class="col-md-12">
                <div class="alert alert-danger">
                    <font color="red">Gagal Unggah File!</font>
                </div>
            </div>';
        }
        else { //jika upload'".$_POST['keterangan']."',
            $sql="INSERT INTO `tbl_bukti_bayar` (`keterangan_file`,`daerah`,`file_name`, `path`, `file_size`, `users_id`, `tgl_unggah`) 
VALUES ('".$_POST['keterangan']."','"._user_daerah()."','".$config["file_name"]."', '".str_replace('../','',$config['upload_path'])."', '".$fileSize."', '"._user_id()."', '".date('Y-m-d H:i:s')."');";
          
            // print_r($temp_data);
            // echo '<div class="col-md-12">
            // <div class="alert alert-info">';
            if(_sq($sql)){
                echo '<div class="col-sm-12">
                        <div class="alert alert-dismissible" style="background-color:#baf2ff;">
                            Berhasil Unggah File
                        </div>
                    </div>';
            }
            else{
                echo '<div class="col-md-12">
                <div class="alert alert-danger">
                    <font color="red">Gagal Unggah File!</font>
                </div>
            </div>';
            }
            
        // }
        }//end jika upload
    }

}

?>
<div class="col-md-12">
    <div class="card md-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Unggah Bukti Bayar (JPG/PNG)</h6>
        </div>
        <div class="card-body">
                                            <form class="user">
                                                
                                                <div class="form-group">
                                                    <label class="small mb-1">Keterangan</label>
                                                    <input type="input" name="keterangan" class="form-control keterangan" placeholder="Keterangan File"/><br>
                                                    <label class="small mb-1">Unggah Foto Bukti Bayar</label><br>
                                                    <input type="file" name="filenya" class="file form-control" placeholder="Unggah Bukti Bayar"/>
                                                </div>
                                            
                                                <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Unggah</a>
                                            </form>
        </div>
    </div>
</div>
<div style="margin:5px;"></div>
<div class="col-md-12">
                            <!-- Import -->
                            <div class="card md-4">
                                <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Bukti Bayar</h6>
                                </div>
                                <div class="card-body table-responsive">
                                    <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No</td>
                                            <td>Keterangan</td>
                                            <td>Tanggal Unggah</td>
                                            <td>File Bukti Bayar</td>
                                            <td>Status Validasi</td>
                                            <td>Tanggal Validasi</td>
                                        </tr>
                                <?php
                                $sql_user = "SELECT * FROM tbl_bukti_bayar where daerah='"._user_daerah()."' order by id desc";
                                $result_user = mysqli_query($koneksi, $sql_user);
                                if(mysqli_num_rows($result_user)> 0) {
                                    $no=1;
                                    while($data_user = mysqli_fetch_array($result_user)){
                                        echo "<tr>
                                        <td>".$no.".</td>
                                        <td>".$data_user['keterangan_file']."</td>c
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_unggah'])."</td>
                                        <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#buktibayar".$data_user['id']."\">Lihat Foto</a></td>
                                        <td>".$data_user['is_valid']."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_validasi'],'-')."</td>
                                        </tr>";
                                        //Modal Foto
                                        $html_foto="<image src='../".$data_user['path']."/".$data_user['file_name']."' width='450'><br/>".$data_user['nama'];
                                        echo modal('buktibayar'.$data_user['id'],'Bukti Bayar',$html_foto);
                                        
                                        
                                        $no++;
                                    }
                                }
                                else{
                                ?>
                                        <tr>
                                            <td colspan="9">- empty -</td>
                                        </tr>
                                <?php
                                }
                                ?>
                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    form_data.append('keterangan', $('.keterangan').val());
                    $.ajax({
                        url: '<?=base_url("proses_unggah_bukti_bayar.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>
