<?php
require '../assets/vendor/autoload.php';

// Include librari PhpSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$spreadsheet = $reader->load('excel.xlsx'); 
$sheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
var_dump($sheet);