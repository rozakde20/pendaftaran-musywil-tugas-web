<?php include('../config/auto_load.php') ?>
<?php include('dashboard_control.php') ?>
<?php include('../template/header.php') ?>
               <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Peserta</h1>
                    
                    <!--<div class="row">-->

                    <!--<?php if(isset($status) && $status == 1) { ?>-->
                            <!-- DIPROSES -->
                    <!--        <div class="col-md-6">-->
                    <!--        <div class="card shadow mb-4">-->
                    <!--            <div class="card-header py-3">-->
                    <!--                <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>-->
                    <!--            </div>-->
                                <!-- DIPROSES PENDAFTARAN -->
                    <!--            <div class="card-body">-->
                    <!--                <div class="card text-center">-->
                    <!--                    <h5 class="card-title mb-3 mt-3">Proses Pendaftaran</h5>-->
                    <!--                    <div class="col-auto">-->
                    <!--                        <i class="fas fa-spinner text-warning" style="font-size: 70px;"></i>-->
                    <!--                    </div>-->
                    <!--                    <p class="card-text">Pendaftaran anda sedang diproses</p>-->
                    <!--                    <span class="badge badge-warning">Pengumuman pada tanggal 10 Agustus</span>-->
                    <!--                </div>               -->
                    <!--            </div>-->
               
                    <!--            </div>-->

                    <!--            <div class="card-footer">-->
                    <!--                <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--        <?php } elseif (isset($status) && $status == 2) { ?>-->

                            <!-- LOLOS -->
                    <!--        <div class="col-md-6">-->
                    <!--        <div class="card shadow mb-4">-->
                    <!--            <div class="card-header py-3">-->
                    <!--                <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>-->
                    <!--            </div>-->
                                <!-- DIPROSES PENDAFTARAN -->
                    <!--            <div class="card-body">-->
                    <!--                <div class="card text-center">-->
                    <!--                    <h5 class="card-title mb-3 mt-3">LOLOS</h5>-->
                    <!--                    <div class="col-auto">-->
                    <!--                        <i class="far fa-check-circle text-success" style="font-size: 70px;"></i>-->
                    <!--                    </div>-->
                    <!--                    <p class="card-text">Selamat Anda Lolos Pendaftaran</p>-->
                    <!--                    <span class="badge bg-gradient-primary">Pengumuman pada tanggal 10 Agustus</span>-->
                    <!--                </div>               -->
                    <!--            </div>-->
               
                    <!--            </div>-->

                    <!--            <div class="card-footer">-->
                    <!--                <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--        <?php } elseif(isset($status) && $status==0) {?>-->

                            <!-- GAGAL -->
                    <!--        <div class="col-md-6">-->
                    <!--        <div class="card shadow mb-4">-->
                    <!--            <div class="card-header py-3">-->
                    <!--                <h6 class="m-0 font-weight-bold text-primary">Status Pendaftaran</h6>-->
                    <!--            </div>-->
                                <!-- DIPROSES PENDAFTARAN -->
                    <!--            <div class="card-body">-->
                    <!--                <div class="card text-center">-->
                    <!--                    <h5 class="card-title mb-3 mt-3">MENUNGGU PEMBAYARAN</h5>-->
                    <!--                    <div class="col-auto">-->
                    <!--                        <i class="fa fa-file-invoice text-danger" style="font-size: 70px;"></i>-->
                    <!--                    </div>-->
                    <!--                    <p class="card-text">SILAHKAN SELESAIKAN REGISTRASI DI HALAMAN PEMBAYARAN</p>-->
                    <!--                    <span class="badge badge-danger">Pengumuman pada tanggal 10 Agustus</span>-->
                    <!--                </div>               -->
                    <!--            </div>-->
               
                    <!--            </div>-->

                    <!--            <div class="card-footer">-->
                    <!--                <marquee style="margin-bottom: -5px">MUSYWIL PEMUDA MUHAMMADIYAH JATENG</marquee>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--        <?php } ?>-->



                            <!-- INFORMASI -->
                    <!--        <div class="col-md-6">-->
                    <!--        <div class="card shadow mb-4">-->
                    <!--            <div class="card-header py-3">-->
                    <!--                <h6 class="m-0 font-weight-bold text-danger">Catatan Penting</h6>-->
                    <!--            </div>-->
                    <!--            <div class="card-body">-->
                    <!--                <p class="text-danger">-->
                    <!--                    Silahkan untuk mengisi dan melengkapi formulir biodata.-->
                    <!--                    Kemudian lakukan pembayaran dan upload bukti pembayaran di bagian pembayaran.-->
                    <!--                    Pendaftaran anda akan diverifikasi terlebih dahulu oleh admin. Status akan berubah menjadi-->
                    <!--                    lolos apabila persyaratan administrasi dan keuangan terverifikasi. Terima Kasih.-->
                    <!--                </p>-->
                                    
                    <!--            </div>-->
                    <!--        </div>-->

                    <!--        </div>-->
                    <!--</div>-->

                    
                    <!-- ROW DIV UNTUK PROFIL DAN FORMULIR -->
                    <div class="row">
                    <!-- FORM PEMBAYARAN -->
                   
                        <div class="col-md-6">                                
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Peserta</h6>
                                </div>
                                <div class="card-body">
                                    <p class="text-primary">
                                        <ol type="1.">
                                            <li>Peserta Peserta Musyawarah Wilayah:
                                              <ol type="a">
                                                  <li>Anggota Musyawarah Wilayah yang terdiri dari:
                                                      <ol type="1)">
                                                      <li>Anggota Pimpinan Wilayah.</li>
                                                      <li>Ketua dan 4 (empat) orang Pimpinan Daerah.</li>
                                                      <li>Ketua dan 2 (dua) orang Pimpinan Cabang.</li>
                                                      </ol>
                                                  </li>
                                                  <li>Wakil Pimpinan Pusat Pemuda Muhammadiyah.</li>
                                                  <li>Wakil Pimpinan Wilayah Muhammadiyah.</li>
                                                  <li>Undangan Pimpinan Wilayah.</li>
                                              </ol>
                                            </li>
                                            <li>Persyaratan Peserta :
                                              <ol type="a">
                                                  <li>Membawa dan Unggah Surat Mandat dari Pimpinan Daerah</li>
                                                  <li>SWO PDPM = Rp 400.000,-</li>
                                                  <li>SWO PCPM = Rp 100.000,-</li>
                                                  <li>SWP = Rp 400.000,-</li>
                                                  <li>Membawa dan Unggah Progress Report Organisasi</li>
                                              </ol>
                                            </li>
                                        </ol>    
                                    </p>
                                    <p class="text-primary">
                                        Pembayaran Melalui Rekening Bank BRI Atas Nama Panitia Musywil Pemuda Muhammadiyah Jawa Tengah 1007-01-007759-53-8
                                        <br/>Pembayaran dilakukan setelah data pendaftaran kolekktif diverifikasi oleh Panitia Musywil.
                                        <br/>Setelah Pembayaran, Admin Daerah unggah bukti pembayaran di menu [Unggah Bukti bayar].
                                    </p>
                                    <p class="text-danger">
                                        Hati-hati! Panitia hanya menyediakan 1 Rekening pembayaran, jika ada rekening lain 
                                        yang mengatasnamakan panitia musywil PM Jateng, harap waspada penipuan! 
                                    </p>
                                </div>
                                <!--<div class="p-5">-->
                                <!--    <form action="" class="user" method="post" enctype="multipart/form-data">-->
                                <!--        <label for="" class="form-group text-gray-900">Sumbangan Wajib Organisasi (SWO)</label>-->
                                <!--        <input type="text" name="swo" class="form-control text-gray-900" readonly placeholder="Rp. 500.000,00">-->
                                <!--        <input type="file" name="swo" id="swo" class="form-control">-->

                                <!--        <label for="" class="form-group text-gray-900 mt-4">Sumbangan Wajib Peserta (SWP)</label>-->
                                <!--        <input type="text" name="swp" class="form-control text-gray-900" readonly placeholder="Rp. 250.000,00">-->
                                <!--        <input type="file" name="swp" id="swp" class="form-control">-->

                                <!--        <div class="text-right"> -->
                                <!--            <button class="btn btn-primary mt-3" name="btn_bayar" id="btn_bayar">-->
                                <!--                UPLOAD-->
                                <!--            </button>-->
                                <!--        </div>-->
                                <!--    </form>-->
                                <!--</div>-->
                            </div>
                            
                        </div>


                        <!-- profil -->
                        <div class="col-md-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-danger">Pendaftaran</h6>
                                </div>
                                <div class="card-body">
                                    <p class="text-primary">
                                        <ol>
                                            <li>Pendaftaran dilakukan kolektif oleh Pimpinan Daerah Pemuda Muhammadiyah (PDPM).</li>
                                            <li>Pimpinan Daerah menunjuk 1 orang untuk menjadi admin (bisa sekretaris/yang mewakili)</li>
                                            <li>Menyiapkan berkas pendaftaran peserta :
                                                <ol type="a">
                                                <li>Surat mandat kolektif dari PDPM</li>
                                                <li>Identitas nama lengkap, jabatan, utusan, No.WhatApp Aktif, NIK dan NBM.</li>
                                                <li>File yang diunggah adalah pas foto berjas Pemuda, KTP, dan NBM</li>
                                                </ol>
                                            </li>
                                            <li>Data dan berkas diisi dan unggah di aplikasi.</li>
                                            <li>Panitia Musywil akan melakukan verifikasi data peserta.</li>
                                            <li>Hasil verifikasi perbaikan data akan dikembalikan ke admin Daerah agar diperbaiki.</li>
                                            <li>Jika Verifikasi dinyatakan berhasil, akan muncul Virtual Akun guna pelakukan pembayaan sesuai jumlah data peserta.</li>
                                            <li>Bukti transfer diunggah ke aplikasi.</li>
                                            <li>Setelah diverifikasi, panitia akan mengirimkan akses akun (user dan password) ke peserta melalui WhatApp masing-masing.</li>
                                            <li>Akses akun peserta meliputi Id Card virtual, jadwal acara, eBook materi, info lokasi acara, jadwal, dsb.</li>
                                            <li>Info lebih lanjut Tim IT Musywil XVIII : 0838 43953264/ 085726922293</li>
                                        </ol>   
                                    </p>
                                </div>
                                
                            </div>
                            
                            <!--<div class="card shadow mb-4">-->
                            <!--    <div class="card-header py-3">-->
                            <!--        <h6 class="m-0 font-weight-bold text-primary">Profil</h6>-->
                            <!--    </div>-->
                            <!--    <div class="card-body">-->
                            <!--        <div class="text-center">-->
                    
                            <!--            <img src="../assets/img/avatar.png"  class="img-fluid rounded-circle" style="width: 200px">-->
                            <!--            <h5 class="text-center card-title">Edi Siswanto</h5>-->
                            <!--            <div class="text-center">-->
                            <!--                <a href="data-diri.php" class="btn btn-warning">Edit Profil</a>-->
                            <!--            </div>-->
                            <!--            <ul class="list-group">-->
                            <!--                <li class="list-group-item">-->
                            <!--                    <h6>Tempat, Tanggal Lahir</h6>-->
                            <!--                    <small>Banyumas, 20 Februari 1991</small>-->
                            <!--                </li>-->
                            <!--                <li class="list-group-item">-->
                            <!--                    <h6>Jabatan</h6>-->
                            <!--                    <small>Sekretaris</small>-->
                            <!--                </li>-->
                            <!--                <li class="list-group-item">-->
                            <!--                    <h6>NBM</h6>-->
                            <!--                    <small>12123444544</small>-->
                            <!--                </li>-->
                            <!--                <li class="list-group-item">-->
                            <!--                    <h6>Asal daerah</h6>-->
                            <!--                    <small>Banyumas</small>-->
                            <!--                </li>-->
                            <!--                <li class="list-group-item">-->
                            <!--                    <h6>Asal Cabang</h6>-->
                            <!--                    <small>Banyuurip</small>-->
                            <!--                </li>-->
                            <!--            </ul>-->
    
                            <!--        </div>-->
                                    
                            <!--        </div>-->
                            <!--    </div>-->
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

 <?php include('../template/footer.php') ?>