<?php 
include('../config/auto_load.php');
// include('../config/koneksi.php');
require '../assets/vendor/autoload.php';
if(!isset($_FILES['filenya'])){
    echo '<div class="col-md-12">
            <div class="alert alert-danger">
                <font color="red">Anda Belum pilih File!</font>
            </div>
        </div>';
    ?>
<div class="col-md-12">
    <div class="card md-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Upload Data Template (XLSX)</h6>
        </div>
        <div class="card-body">
                                            <form class="user">
                                                
                                                <div class="form-group">
                                                    <label for="">Upload Data Kolektif sesuai template</label><br>
                                                    <input type="file" name="filenya" class="file" placeholder="Upload Data"/>
                                                </div>
                                            
                                                <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Import</a>
                                                <a class="btn btn-danger mb-5" href="daftar_kolektif.php" title="klik untuk kembali">Kembali</a>
                                            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    $.ajax({
                        url: '<?=base_url("proses_import_utusan.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>
    <?php 
    die();
} //end jika file kosong

    $date_data=date('Y-m-d H:i:s');
    // Config File upload lampiran F
    $rename_dokumen='Utusan_';
    $fileName = $_FILES['filenya']['name'];
    $tmpName  = $_FILES['filenya']['tmp_name'];
    $fileSize = $_FILES['filenya']['size'];
    $fileType = $_FILES['filenya']['type'];
    $filediijinkan=array(
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-excel'
    );

if(!in_array($fileType, $filediijinkan) ){ 
        echo '<div class="col-md-12">
            <div class="alert alert-danger">
                <font color="red">File "'.$fileName.'" Tidak diijinkan! <br/>File yang diijinkan (.XLSX/.XLS), silahkan unduh dari template.</font>
            </div>
        </div>';
}
elseif(isset($_FILES['filenya']) && in_array($fileType, $filediijinkan) && $fileName!=''){
    $spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($tmpName);
    $reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($tmpName);    
    
    $rename_dokumen='Utusan_'.$spreadsheet->getActiveSheet()->getCell('B1')->getValue().'_';
    $config['upload_path']          = '../assets/uploads/dokumen_excel_utusan/';
    $config['allowed_types']        = '*';
    $config['file_name']            = $rename_dokumen.time().".xlsx";
    $config['overwrite']			= true;
    $config['max_size']             = 2*MB;//2000; // 1MB
    
    if ($fileSize > $config['max_size'] ) {
        echo '<div class="col-md-12">
                <div class="alert alert-danger">
                    <font color="red">File melebihi batas! max 2MB File Anda '.round(($fileSize/MB),4).' MB</font>
                </div>
            </div>';
    }
    else 
    if($spreadsheet->getActiveSheet()->getCell('A3')->getValue()!='Nama Lengkap'){ 
        echo '<div class="col-md-12">
            <div class="alert alert-danger">
                <font color="red">Template Salah! Silahkan hubungi Admin</font>
            </div>
        </div>';
    }
    else{
        if(!move_uploaded_file($tmpName, $config['upload_path'].$config["file_name"])){
            echo '<div class="col-md-12">
                <div class="alert alert-danger">
                    <font color="red">Template Salah! Silahkan hubungi Admin</font>
                </div>
            </div>';
        }
        else { //jika upload
        $rai=4;$dsi=0;$dgi=0;$du=0;
        // foreach($spreadsheet->getWorksheetIterator() as $worksheet)
        // {
            $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();//$worksheet->getHighestRow();
            $highestColumn = $spreadsheet->getActiveSheet()->getHighestColumn();//$worksheet->getHighestColumn();
            $y=0;//banyaknya  data yg diisi
            for($r=$rai; $r<=$highestRow; $r++){
                if($spreadsheet->getActiveSheet()->getCell('A'.$r)->getValue()!='' and $spreadsheet->getActiveSheet()->getCell('B'.$r)->getValue()!=''){
                    $y++;
                }
            }
            $raia=(($rai-1)+$y);
            for($row=$rai; $row<=$raia; $row++){
                $temp_data[$row] = array(
                        	'nama'	        => $spreadsheet->getActiveSheet()->getCell('A'.$row)->getValue(),
                            'jabatan'       => $spreadsheet->getActiveSheet()->getCell('B'.$row)->getValue(),
                            'kode_pimpinan' => $spreadsheet->getActiveSheet()->getCell('C'.$row)->getValue(),
                            'pimpinan'      => $spreadsheet->getActiveSheet()->getCell('D'.$row)->getValue(),
            	            'daerah'        => $spreadsheet->getActiveSheet()->getCell('E'.$row)->getValue(),
            	            'cabang'        => $spreadsheet->getActiveSheet()->getCell('F'.$row)->getValue(),
            	            'nik'           => substr($spreadsheet->getActiveSheet()->getCell('G'.$row)->getValue(),0,16),
            	            'nbm'           => $spreadsheet->getActiveSheet()->getCell('H'.$row)->getValue(),
            	            'tmpt_lahir'    => $spreadsheet->getActiveSheet()->getCell('I'.$row)->getValue(),
            				// 'tgl_lahir'   => $spreadsheet->getActiveSheet()->getCell('J'.$row)->getValue(),
            				'tgl_lahir'     => sesuaikan_tgl($spreadsheet->getActiveSheet()->getCell('J'.$row)->getValue()),
            				'nomor'         => $spreadsheet->getActiveSheet()->getCell('K'.$row)->getValue(),
            				'email'         => $spreadsheet->getActiveSheet()->getCell('L'.$row)->getValue(),
            				'url_foto'      => $spreadsheet->getActiveSheet()->getCell('M'.$row)->getValue(),
            				'url_ktp'       => $spreadsheet->getActiveSheet()->getCell('N'.$row)->getValue(),
            				'url_nbm'       => $spreadsheet->getActiveSheet()->getCell('O'.$row)->getValue(),
                            'file'          => $config['file_name'],
            				//'kaos'         => $spreadsheet->getActiveSheet()->getCell('K'.$row)->getValue(),
            				'tgl_daftar'    => $date_data,
            				'user_daerah'    => _user_daerah(),
            				'hapus'         => '0',
            				'users_id'      => _user_id(),
                        );
                // $field=array();$val=$array();
                // var_dump($temp_data);die();
                $aa=1;
                $field[$row]='';#$value='';
                foreach($temp_data[$row] as $x => $y){
                    if($aa==count($temp_data[$row])){$s='';}else{$s=',';}    
                    $field[$row].=$x.$s;
                    if($x=='tgl_lahir'){
                        $value[$row][]="'".$y."'";
                    }
                    else{
                        $value[$row][]="'".addslashes($y)."'";
                    }
                    $f[$row][]=$x;
                    $v[$row][]=$y;
                    $aa++;
                }
                // echo implode(',',$field);
                // echo implode(',',$val);

                
                //echo $sql_data;
                $import = import($field[$row], $value[$row],$f[$row],$v[$row]);
                // echo $import;
                if($import==1){$dsi+=1;}//insert
                elseif($import==2){$du+=1;}//update
                else{$dgi+=1;}
            }
            // print_r($temp_data);
            // echo '<div class="col-md-12">
            // <div class="alert alert-info">';
            echo '<div class="col-sm-12">
                    <div class="alert alert-dismissible" style="background-color:#baf2ff;">
                        <ul>
                            <li style="color:green;">Data Baru: '.$dsi.'</li>
                            <li style="color:green;">Data Update: '.$du.'</li>
                            <li style="color:red;">Data Gagal: '.$dgi.  '</li>
                        </ul>
                    </div>
                </div>';
            
        // }
        }//end jika upload
    }

}

else{
    echo '<div class="col-md-6">
                <div class="alert alert-danger">
                    <font color="red">Silahkan Upload Data Excel sesuai template!</font>
                </div>              
            </div>';
}?>
<div class="col-md-12">
<div class="card md-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Upload Data Template (XLSX)</h6>
    </div>
    <div class="card-body">
                                        <form class="user">
                                            
                                            <div class="form-group">
                                                <label for="">Upload Data Kolektif sesuai template</label><br>
                                                <input type="file" name="filenya" class="file" placeholder="Upload Data"/>
                                            </div>
                                        
                                            <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Import</a>
                                            <a class="btn btn-danger mb-5" href="daftar_kolektif.php" title="klik untuk kembali">Kembali</a>
                                        </form>
    </div>
</div>
</div>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    $.ajax({
                        url: '<?=base_url("proses_import_utusan.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>
