<?php
// include('../config/koneksi.php');

$id_user = $_SESSION['id_users'];
$result_id = "SELECT * FROM pendaftar WHERE users_id = '$id_user'";
$result_pendaftar = mysqli_query($koneksi, $result_id);

if(mysqli_num_rows($result_pendaftar)) {
    $data_pendaftar = mysqli_fetch_array($result_pendaftar);
    $id_pendaftar = $data_pendaftar['users_id'];

    $sql_upload = "SELECT * FROM upload WHERE pendaftar_id = '$id_pendaftar'";
    $result_upload = mysqli_query($koneksi, $sql_upload);

    if(mysqli_num_rows($result_upload)) {
        $data_upload = mysqli_fetch_array($result_upload);
        $status = $data_upload['status'];
        $pas_foto = $data_upload['pas_foto'];


      } else  {
        // jika belum ada data nilai/ status maka kosong
    }
}



if(isset($_POST['btn_simpan'])) {

  // Mendapatkan nilai-nilai dari form
  $ktp = generateUniqueName($_FILES['ktp']['name']);
  $nbm = generateUniqueName($_FILES['nbm']['name']);
  $pas_foto = generateUniqueName($_FILES['pas_foto']['name']);
  $surat_mandat = generateUniqueName($_FILES['surat_mandat']['name']);
  $sk_pimpinan = generateUniqueName($_FILES['sk_pimpinan']['name']);

  // Memeriksa ukuran file
  if ($_FILES['ktp']['size'] <= 2 * 1024 * 1024 && $_FILES['nbm']['size'] <= 2 * 1024 * 1024 && $_FILES['pas_foto']['size'] <= 2 * 1024 * 1024 && $_FILES['surat_mandat']['size'] <= 2 * 1024 * 1024  && $_FILES['sk_pimpinan']['size'] <= 2 * 1024 * 1024) {
    // Memindahkan file gambar ke folder yang diinginkan
    move_uploaded_file($_FILES['ktp']['tmp_name'], "../uploads/ktp/" . $ktp);
    move_uploaded_file($_FILES['nbm']['tmp_name'], "../uploads/nbm/" . $nbm);
    move_uploaded_file($_FILES['pas_foto']['tmp_name'], "../uploads/pas_foto/" . $pas_foto);
    move_uploaded_file($_FILES['surat_mandat']['tmp_name'], "../uploads/sk_pimpinan/" . $surat_mandat);
    move_uploaded_file($_FILES['sk_pimpinan']['tmp_name'], "../uploads/surat_mandat/" . $sk_pimpinan);

    // Menyimpan data ke database
    $query = "INSERT INTO upload(foto_ktp, foto_nbm, pas_foto, surat_mandat, sk_pimpinan, pendaftar_id)
              VALUES ('$ktp', '$nbm', '$pas_foto', '$surat_mandat', '$sk_pimpinan', '$id_user')";

    if (mysqli_query($koneksi, $query)) {
      $_SESSION['pesan_upload'] = 'UPLOAD BERHASIL';
      header('location:dashboard.php');
    } else {
      echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
    }
  } else {
    $_SESSION['error_upload'] = 'Ukuran File lebih dari 2 MB';
    header('location:dashboard.php');
  }

  // Menutup koneksi

  
}


// Mengupload file pembayaran
if(isset($_POST['btn_bayar'])) {
    $swo = generateUniqueName($_FILES['swo']['name']);
    $swp = generateUniqueName($_FILES['swp']['name']);

    if($_FILES['swo']['size'] <= 2 * 1024 * 1024 && $_FILES['swp']['size'] <= 2 * 1024 * 1024) {
    // Memindahkan file gambar ke folder yang diinginkan
    move_uploaded_file($_FILES['swo']['tmp_name'], "../uploads/swo/" . $swo);
    move_uploaded_file($_FILES['swp']['tmp_name'], "../uploads/swp/" . $swp);

    $query_bayar = "UPDATE upload SET swp = '$swo', swo = '$swp', status = 1 WHERE pendaftar_id = '$id_user'";

    if (mysqli_query($koneksi, $query_bayar)) {
        $_SESSION['pesan_upload'] = 'UPLOAD BERHASIL';
        header('location:dashboard.php');
      } else {
        echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
      }
    } else {
      $_SESSION['error_upload'] = 'Ukuran File lebih dari 2 MB';
      header('location:dashboard.php');
    }
  
    // Menutup koneksi
    mysqli_close($koneksi);

    }




// Fungsi untuk menghasilkan nama unik
  function generateUniqueName($filename) {
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    $allowedExtensions = array("png", "jpg", "jpeg");
  
    // Memeriksa ekstensi file
    if (in_array($ext, $allowedExtensions)) {
      $randomName = uniqid() . '.' . $ext;
  
      // Memeriksa apakah nama file sudah ada di direktori
      while (file_exists("folder_gambar/" . $randomName)) {
        $randomName = uniqid() . '.' . $ext;
      }
  
      return $randomName;
    } else {
      // Jika ekstensi tidak diperbolehkan, kembalikan false atau lakukan penanganan error sesuai kebutuhan
      return false;
    }
}



?>
