<?php include('../config/auto_load.php');
include('../template/header.php'); 
$id_kirim_data=_row('data_kirim','id,daerah','id','daerah="'._user_daerah().'" and status_kirim="1"');
if($id_kirim_data>0){
    echo '<div class="modal fade show" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="padding-right: 17px; display: block;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Peringatan:</h5>
                </div>
                <div class="modal-body text-danger">Data Peserta sudah dikirim!<br/>Import data tidak bisa dilakukan lagi.</div>
                <div class="modal-footer">
                    <a class="btn btn-primary" href="https://netnusantara.id/musywil/peserta/daftar_kolektif">OK</a>
                </div>
            </div>
        </div>
    </div><div class="modal-backdrop fade show"></div>';
    die();
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Form Import Data Utusan</h1>
                    <div class="row unggah">
                        <div class="col-md-12">
                            <!-- Import -->
                            <div class="card md-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Upload Data Template (XLSX)</h6>
                                    </div>
                                    <div class="card-body">
                                        <form class="user">
                                            
                                            <div class="form-group">
                                                <label for="">Upload Data Kolektif sesuai template</label><br>
                                                <input type="file" name="filenya" class="file" placeholder="Upload Data"/>
                                            </div>
                                        
                                            <a class="btn btn-primary mb-5 simpan" title="Klik untuk mulai proses import data">Proses Import</a>
                                            <a class="btn btn-danger mb-5" href="daftar_kolektif.php" title="klik untuk kembali">Kembali</a>
                                        </form>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>
<script>
$(document).ready(function(){
    $('.simpan').on('click', function () {
                    var file_data = $('.file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('filenya', file_data);
                    $.ajax({
                        url: '<?=base_url("proses_import_utusan.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('.unggah').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('.unggah').html(response); // display error response from the server
                        }
                    });
    });
});
</script>