<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Detail Peserta</h1>
    <div class="row">
        <div class="col-md-5">
            <div class="card md-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-id-card"></i> Identitas Peserta</h6>
                </div>
                <div class="card-body">
                        <?php
                            $sql_user = "SELECT * FROM pendaftar where nbm='"._smgdec($_GET['id'])."'";
                            $result_user = mysqli_query($koneksi, $sql_user);
                        if(mysqli_num_rows($result_user)> 0) {
                            while($data_user = mysqli_fetch_array($result_user)){
                            echo '<div class="form-group">
                                    <label for=""><b>Nama</b></label> '.$data_user['nama'].'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>NBM</b></label> '.$data_user['nbm'].'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>Jabatan</b></label> '.$data_user['jabatan'].'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>Daerah</b></label> '.nama_daerah($data_user['daerah']).'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>Cabang</b></label> '.nama_cabang($data_user['cabang']).'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>Nomor Whatsapp</b></label> '.$data_user['nomor'].'
                                </div>';
                            echo '<div class="form-group">
                                    <label for=""><b>Email</b></label> '.$data_user['email'].'
                                </div>';
                            }
                        }
                        ?>         
                </div>
            </div>
        </div>
        
        <div class="col-md-5">
            <div class="card md-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-file-upload"></i> Upload Dokumen</h6>
                </div>
                <div class="card-body">
                    <form class="user">
                        <?='<input type="hidden" nbm="'._smgdec($_GET['id']).'">';?>            
                        <div class="form-group">
                            <label for="">Upload Foto KTP</label><br>
                            <input type="file" name="ktp" placeholder="Upload Foto KTP"/>
                        </div>
                        <div class="form-group">
                            <label for="">Upload foto NBM</label><br>
                            <input type="file" name="nbm" placeholder="Upload Foto NBM"/>
                        </div>
                        <div class="form-group">
                            <label for="">Upload Pas foto</label><br>
                            <input type="file" name="pas-foto" placeholder="Upload Pas Foto"/>
                        </div>
                        <div class="form-group">
                            <label for="">Upload Surat Mandat</label><br>
                            <input type="file" name="pas-foto" placeholder="Upload surat mandat"/>
                        </div>
                        <div class="form-group">
                            <label for="">Upload SK Pimpinan</label><br>
                            <input type="file" name="pas-foto" placeholder="Upload SK Pimpinan"/>
                        </div>
                                                    
                            <a class="btn btn-primary mb-5 upload" title="Klik untuk mulai upload">Upload</a>
                            <a class="btn btn-danger mb-5" href="daftar_kolektif.php" title="klik untuk kembali">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>


<?php include('../template/footer.php') ?>