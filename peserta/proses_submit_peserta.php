<?php 
include('../config/auto_load.php');
include('../template/header.php');
$id_daerah=_smgdec($_GET['id']);
$daerah=_row('data_kirim','daerah','daerah','daerah="'.$id_daerah.'" and status_kirim="0"');
$field="`daerah`,`status_kirim`,`tgl_kirim`,`user_id`";
$value=array($id_daerah,'1',date('Y-m-d H:i:s'),_user_id());
$sql_update="UPDATE data_kirim set status_kirim='1', tgl_kirim='".date('Y-m-d H:i:s')."', user_id='"._user_id()."' where daerah='".$id_daerah."';";
if($daerah>0){
    $action='kirim ulang peserta';
    $log=$sql_update;
    if(_sq($sql_update) && catatan($action,$log)){
        echo '<div class="modal fade show" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="padding-right: 17px; display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Success</h5>
                    </div>
                    <div class="modal-body">Data Berhasil dikirim!</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="'.base_url('daftar_kolektif').'">OK</a>
                    </div>
                </div>
            </div>
        </div><div class="modal-backdrop fade show"></div>';
    }
    else{
        echo '<div class="modal fade show" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="padding-right: 17px; display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Failed!</h5>
                    </div>
                    <div class="modal-body text-danger">Data Gagal dikirim!</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="'.base_url('daftar_kolektif').'">OK</a>
                    </div>
                </div>
            </div>
        </div><div class="modal-backdrop fade show"></div>';
    }
}
else{
    $action='kirim peserta';
    $log=$field.' '.implode(',',$value);
    if(kirim_data($field,$value) && catatan($action,$log)){
        echo '<div class="modal fade show" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="padding-right: 17px; display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Success</h5>
                    </div>
                    <div class="modal-body">Data Berhasil dikirim!</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="'.base_url('daftar_kolektif').'">OK</a>
                    </div>
                </div>
            </div>
        </div><div class="modal-backdrop fade show"></div>';
    }
    else{
        echo '<div class="modal fade show" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="padding-right: 17px; display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Failed!</h5>
                    </div>
                    <div class="modal-body text-danger">Data Gagal dikirim!</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="'.base_url('daftar_kolektif').'">OK</a>
                    </div>
                </div>
            </div>
        </div><div class="modal-backdrop fade show"></div>';
    }
}
?>