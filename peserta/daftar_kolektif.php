<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>
<?php
    $id_kirim_data=_row('data_kirim','id,daerah','id','daerah="'._user_daerah().'" and status_kirim="1"');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Pendaftar</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($id_kirim_data>0){?>
                            <div class="alert alert-success">
                                <b>Data telah dikirim.</b> Silahkan tunggu sampai proses verifikasi. Admin Daerah akan mendapatkan notifikasi untuk melakukan pembayaran setelah verifikasi dari Panitia selesai.
                            </div>
                            <?php } else{?>
                            <div class="alert alert-info">
                                <b>Perhatian!</b> Silahkan unduh template pengisian data ini pada alamat berikut: <a target="_blank" href="https://docs.google.com/spreadsheets/d/19Sj8Jj36KH-S5BamIgUowp3SgfVl--Qc/edit" title="Klik untuk download template">Template</a>. Pastikan Saudara mengisi template tersebut dengan memperhatikan catatan pada kolom pengisian.
                            </div>
                            <?php }
                            if($id_kirim_data>0){}else{?>
                            <a href="import_data" class="btn btn-primary alert"><i class="fas fa-upload"></i> Import Data Kolektif</a>
                            <a href="kirim_data" data-toggle="modal" data-target="#save" class="btn btn-danger alert"><i class="fas fa-save"></i> Kirim Data</a>
                            <?php
                            }
                            $html_foto="Apakah Anda yakin akan mengirim data peserta?<br/><font color='red'>*)Data yang sudah dikirim tidak dapat dibatalkan atau dihapus</font>";
                            $btn='<button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button> 
                            <a href="'.home_base_url().'peserta/kirim-'._smgenc(_user_daerah()).'" class="btn btn-danger btn-xs">Ya</a>';
                            echo modal('save','Konfirmasi',$html_foto,'',$btn);
                            ?>
                        </div>

                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="card-header"><?php if($id_kirim_data>0){?><i class="fas fa-save"></i> Data Utusan Daerah/Cabang <span class="badge bg-teal">⏱️️ proses verifikasi..</span><?}else{?>Data Utusan Daerah/Cabang<?}?></div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <tr>
                                                <td>No</td>
                                                <td>Nama</td>
                                                <td>NBM</td>
                                                <td>Jabatan</td>
                                                <td>Utusan</td>
                                                <td>Daerah</td>
                                                <td>Cabang</td>
                                                <td>Whatsapp</td>
                                                <td>Email</td>
                                                <td>Foto</td>
                                                <td>KTP</td>
                                                <td>NBM</td>
                                                <td>Aksi</td>
                                            </tr>
                                    <?php
                                    $sql_user = "SELECT * FROM pendaftar where hapus!='1' and user_daerah='"._user_daerah()."'";
                                    $result_user = mysqli_query($koneksi, $sql_user);
                                    if(mysqli_num_rows($result_user)> 0) {
                                        $no=1;
                                        while($data_user = mysqli_fetch_array($result_user)){
                                            echo "<tr>
                                            <td>".$no.".</td>
                                            <td>".$data_user['nama']."</td>
                                            <td>".$data_user['nbm']."</td>
                                            <td>".$data_user['jabatan']."</td>
                                            <td>".$data_user['pimpinan']."</td>
                                            <td>".nama_daerah($data_user['daerah'])."</td>
                                            <td>".nama_cabang($data_user['cabang'])."</td>
                                            <td>".$data_user['nomor']."</td>
                                            <td>".$data_user['email']."</td>
                                            <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#foto".$data_user['nbm']."\">Lihat Foto</a></td>
                                            <td><a href='".$data_user['url_ktp']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#ktp".$data_user['nbm']."\">Lihat KTP</a></td>
                                            <td><a href='".$data_user['url_nbm']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#nbm".$data_user['nbm']."\">Lihat NBM</a></td>
                                            <td>".
                                            (($id_kirim_data>0)?"<i class=\"fas fa-save\"></i> Data terkirim":"
                                            <a href=\"".home_base_url()."peserta/hapus-"._smgenc($data_user['nbm'])."\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash\"></i> Hapus</a>")
                                            ."</td>
                                            </tr>";// <td><a href=\"".home_base_url()."peserta/detail-"._smgenc($data_user['nbm'])."\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-user\"></i> Detail</a></td>
                                            //Modal Foto
                                            $html_foto="<image src='".$data_user['url_foto']."'><br/>".$data_user['nama'];
                                            echo modal('foto'.$data_user['nbm'],'Foto Peserta NBM.'.$data_user['nbm'],$html_foto);
                                            
                                            //Modal KTP
                                            $html_ktp="<image src='".$data_user['url_ktp']."'><br/>".$data_user['nama'];
                                            echo modal('ktp'.$data_user['nbm'],'Foto KTP NBM.'.$data_user['nbm'],$html_ktp);
                                            
                                            //Modal KTP
                                            $html_nbm="<image src='".$data_user['url_nbm']."'><br/>".$data_user['nama'];
                                            echo modal('nbm'.$data_user['nbm'],'Foto NBM.'.$data_user['nbm'],$html_nbm);
                                            
                                            $no++;
                                        }
                                    }
                                    else {
                                    ?>
                                            <tr>
                                                <td colspan="9">- empty -</td>
                                            </tr>
                                    <?php
                                    }
                                    ?>
                                        </table>
    
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.container-fluid -->
                

<?php include('../template/footer.php') ?>
<script>
$(document).ready(function(){
    $('.datatable-table').DataTable();
});
</script>