<?php
include('config/koneksi.php');
session_start();

$secret_key = "6LevyhknAAAAAJsCKT7VCI9aQZyBm6T_6VhHuQdg";
// Disini kita akan melakukan komunkasi dengan google recpatcha
// dengan mengirimkan scret key dan hasil dari response recaptcha nya
$verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
$response = json_decode($verify);

if(isset($_POST['btn_login'])) {
// Jika proses validasi captcha berhasil
        // echo "btn ditekan";
        $username = $_POST['email'];
        $password = ($_POST['password']);
        $salt='yd2smg';
        // $p=password_hash($password.$salt, PASSWORD_BCRYPT,['cost' => 9]);
    
        $sql_user = "SELECT * FROM users where username = '$username'";
        $pwd=_sq($sql_user)->fetch_object()->password;
        $result_user = mysqli_query($koneksi, $sql_user);
        

        if(mysqli_num_rows($result_user) > 0 && password_verify($password.$salt, $pwd)) {
            // jika data tersedia, simpan data user kedalam session
            while($data_user = mysqli_fetch_array($result_user)){
                $_SESSION['status'] = 'login';
                $_SESSION['id_users'] = $data_user['id'];
                $_SESSION['nama'] = $data_user['nama'];
                $_SESSION['level'] = $data_user['level'];
                $_SESSION['daerah'] = $data_user['daerah'];
                $_SESSION['level_user'] = $data_user['level_user'];
    
                if($data_user['level'] == 'admin') {
                    header('location:admin/dashboard.php');
    
                } 
                else if($data_user['level'] == 'peserta') {
                    header('location:peserta/dashboard.php');
    
                }
                else if($data_user['level'] == 'utusan') {
                    header('location:utusan/dashboard.php');
    
                } 
    
            }
        } else {
            $_SESSION['login_error'] = "Username atau Password anda Salah!";
            header('location:login.php');
        }


} else {
    header('location:login');
}
