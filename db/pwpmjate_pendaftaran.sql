-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 17, 2023 at 10:05 PM
-- Server version: 5.7.42-cll-lve
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pwpmjate_pendaftaran`
--

-- --------------------------------------------------------

--
-- Table structure for table `catatan`
--

CREATE TABLE `catatan` (
  `id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catatan`
--

INSERT INTO `catatan` (`id`, `action`, `catatan`, `user_id`, `time`) VALUES
(1, 'kirim peserta', '`daerah`,`status_kirim`,`tgl_kirim`,`user_id` 3301,1,2023-07-17 08:48:40,7', 7, '2023-07-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `daerah_selesai_verifikasi`
--

CREATE TABLE `daerah_selesai_verifikasi` (
  `id` int(11) NOT NULL,
  `daerah` varchar(4) NOT NULL,
  `nominal` int(9) NOT NULL,
  `status_selesai` enum('0','1') NOT NULL DEFAULT '1',
  `tgl_selesai` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_kirim`
--

CREATE TABLE `data_kirim` (
  `id` int(11) NOT NULL,
  `daerah` varchar(4) NOT NULL,
  `status_kirim` enum('0','1') NOT NULL DEFAULT '1',
  `tgl_kirim` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kirim`
--

INSERT INTO `data_kirim` (`id`, `daerah`, `status_kirim`, `tgl_kirim`, `user_id`) VALUES
(1, '3301', '1', '2023-07-17 08:48:40', 7);

-- --------------------------------------------------------

--
-- Table structure for table `pendaftar`
--

CREATE TABLE `pendaftar` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `nbm` varchar(20) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `kode_pimpinan` enum('1','2','3') DEFAULT NULL,
  `pimpinan` varchar(20) DEFAULT NULL,
  `daerah` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `tmpt_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nomor` varchar(45) DEFAULT NULL,
  `url_foto` varchar(255) DEFAULT NULL,
  `url_ktp` varchar(255) DEFAULT NULL,
  `url_nbm` varchar(255) DEFAULT NULL,
  `kaos` varchar(5) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tgl_daftar` datetime DEFAULT NULL,
  `user_daerah` varchar(4) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `hapus` enum('0','1') NOT NULL DEFAULT '0',
  `tgl_hapus` datetime DEFAULT NULL,
  `users_id_hapus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftar`
--

INSERT INTO `pendaftar` (`id`, `nama`, `nik`, `nbm`, `jabatan`, `kode_pimpinan`, `pimpinan`, `daerah`, `cabang`, `tmpt_lahir`, `tgl_lahir`, `email`, `nomor`, `url_foto`, `url_ktp`, `url_nbm`, `kaos`, `file`, `tgl_daftar`, `user_daerah`, `users_id`, `hapus`, `tgl_hapus`, `users_id_hapus`) VALUES
(1, 'Muhammad al-Fatih', '3301078110270001', '110737171261151', 'Ketua', '2', 'PDPM KabupatenÂ Cila', '3301', '', 'Semarang', '1981-10-27', 'dummyemail1@example.com', '6285123456751', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(2, 'Ahmad Hidayat', '3301078110270002', '110737171261152', 'Sekretaris', '2', 'PDPM KabupatenÂ Cila', '3301', '', 'Semarang', '1982-10-27', 'testemail2@example.com', '6285123456752', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(3, 'Abdul Malik', '3301078110270003', '110737171261153', 'Bendahara', '2', 'PDPM KabupatenÂ Cila', '3301', '', 'Semarang', '1983-10-27', 'sandboxemail3@example.com', '6285123456753', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(4, 'Muhammad Azhar', '3301078110270004', '110737171261154', 'Bendahara', '2', 'PDPM KabupatenÂ Cila', '3301', '', 'Semarang', '1984-10-27', 'tempemail4@example.com', '6285123456754', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(5, 'Firdaus Yusuf', '3301078110270005', '110737171261155', 'Ketua', '3', 'PCPMÂ Kedungreja', '3301', '330101', 'Semarang', '1985-10-27', 'trialmail5@example.com', '6285123456755', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(6, 'Miftahul Amin', '3301078110270006', '110737171261156', 'Sekretaris', '3', 'PCPMÂ Kedungreja', '3301', '330101', 'Semarang', '1986-10-27', 'dummyaccount6@example.com', '6285123456756', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(7, 'Rohmatullah Hakim', '3301078110270007', '110737171261157', 'Ketua', '3', 'PCPM Kesugihan', '3301', '330102', 'Semarang', '1987-10-27', 'testuser7@example.com', '6285123456757', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(8, 'Zainul Arifin', '3301078110270008', '110737171261158', 'Sekretaris', '3', 'PCPM Kesugihan', '3301', '330102', 'Semarang', '1988-10-27', 'tempmail8@example.com', '6285123456758', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(9, 'Andi Jaya', '3301078110270009', '110737171261159', 'Ketua', '3', 'PCPMÂ Adipala', '3301', '330103', 'Semarang', '1989-10-27', 'sandboxuser9@example.com', '6285123456759', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(10, 'Hasanuddin Saleh', '3301078110270010', '110737171261160', 'Sekretaris', '3', 'PCPMÂ Adipala', '3301', '330103', 'Semarang', '1990-10-27', 'exampledummy10@example.com', '6285123456760', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '0', NULL, NULL),
(11, 'Ahmad Soleh', '3301078110270011', '110737171261161', 'Anggota', '3', 'PCPMÂ Adipala', '3301', '330103', 'Semarang', '1991-10-27', 'email@pwpmjateng.or.id', '6285123456761', 'https://bootdey.com/img/Content/avatar/avatar7.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI4fhB_iqIpl3U08u8qOxR6yU7gvXSK6uHJQ&usqp=CAU&reload=on', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTah8xkkBNPaiVBeYlw6b0wNsjEIyci0BY5LlL3_fh8QExkirGoQlZDqZ9iPJlx9X0-v1M&usqp=CAU', NULL, 'Utusan__1689375040.xlsx', '2023-07-15 05:50:39', '3301', 7, '1', '2023-07-17 08:25:36', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bukti_bayar`
--

CREATE TABLE `tbl_bukti_bayar` (
  `id` int(11) NOT NULL,
  `daerah` int(4) NOT NULL,
  `keterangan_file` varchar(255) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `file_size` varchar(30) NOT NULL,
  `users_id` int(11) NOT NULL,
  `tgl_unggah` datetime DEFAULT NULL,
  `is_valid` enum('0','1') DEFAULT '0',
  `tgl_validasi` datetime DEFAULT NULL,
  `user_validasi` int(11) DEFAULT NULL,
  `keterangan_validasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cabang`
--

CREATE TABLE `tbl_cabang` (
  `kode_cabang` int(6) NOT NULL,
  `nama_cabang` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tbl_cabang`
--

INSERT INTO `tbl_cabang` (`kode_cabang`, `nama_cabang`) VALUES
(330101, 'Kedungreja'),
(330102, 'Kesugihan'),
(330103, 'Adipala'),
(330104, 'Binangun'),
(330105, 'Nusawungu'),
(330106, 'Kroya'),
(330107, 'Maos'),
(330108, 'Jeruklegi'),
(330109, 'Kawunganten'),
(330110, 'Gandrungmangu'),
(330111, 'Sidareja'),
(330112, 'Karangpucung'),
(330113, 'Cimanggu'),
(330114, 'Majenang'),
(330115, 'Wanareja'),
(330116, 'Dayeuhluhur'),
(330117, 'Sampang'),
(330118, 'Cipari'),
(330119, 'Patimuan'),
(330120, 'Bantarsari'),
(330121, 'Cilacap Selatan'),
(330122, 'Cilacap Tengah'),
(330123, 'Cilacap Utara'),
(330124, 'Kampung Laut'),
(330201, 'Lumbir'),
(330202, 'Wangon'),
(330203, 'Jatilawang'),
(330204, 'Rawalo'),
(330205, 'Kebasen'),
(330206, 'Kemranjen'),
(330207, 'Sumpiuh'),
(330208, 'Tambak'),
(330209, 'Somagede'),
(330210, 'Kalibagor'),
(330211, 'Banyumas'),
(330212, 'Patikraja'),
(330213, 'Purwojati'),
(330214, 'Ajibarang'),
(330215, 'Gumelar'),
(330216, 'Pekuncen'),
(330217, 'Cilongok'),
(330218, 'Karanglewas'),
(330219, 'Sokaraja'),
(330220, 'Kembaran'),
(330221, 'Sumbang'),
(330222, 'Baturraden'),
(330223, 'Kedungbanteng'),
(330224, 'Purwokerto Selatan'),
(330225, 'Purwokerto Barat'),
(330226, 'Purwokerto Timur'),
(330227, 'Purwokerto Utara'),
(330301, 'Kemangkon'),
(330302, 'Bukateja'),
(330303, 'Kejobong'),
(330304, 'Kaligondang'),
(330305, 'Purbalingga'),
(330306, 'Kalimanah'),
(330307, 'Kutasari'),
(330308, 'Mrebet'),
(330309, 'Bobotsari'),
(330310, 'Karangreja'),
(330311, 'Karanganyar'),
(330312, 'Karangmoncol'),
(330313, 'Rembang'),
(330314, 'Bojongsari'),
(330315, 'Padamara'),
(330316, 'Pengadegan'),
(330317, 'Karangjambu'),
(330318, 'Kertanegara'),
(330401, 'Susukan'),
(330402, 'Purworeja Klampok'),
(330403, 'Mandiraja'),
(330404, 'Purwanegara'),
(330405, 'Bawang'),
(330406, 'Banjarnegara'),
(330407, 'Sigaluh'),
(330408, 'Madukara'),
(330409, 'Banjarmangu'),
(330410, 'Wanadadi'),
(330411, 'Rakit'),
(330412, 'Punggelan'),
(330413, 'Karangkobar'),
(330414, 'Pagentan'),
(330415, 'Pejawaran'),
(330416, 'Batur'),
(330417, 'Wanayasa'),
(330418, 'Kalibening'),
(330419, 'Pandanarum'),
(330420, 'Pagedongan'),
(330501, 'Ayah'),
(330502, 'Buayan'),
(330503, 'Puring'),
(330504, 'Petanahan'),
(330505, 'Klirong'),
(330506, 'Buluspesantren'),
(330507, 'Ambal'),
(330508, 'Mirit'),
(330509, 'Prembun'),
(330510, 'Kutowinangun'),
(330511, 'Alian'),
(330512, 'Kebumen'),
(330513, 'Pejagoan'),
(330514, 'Sruweng'),
(330515, 'Adimulyo'),
(330516, 'Kuwarasan'),
(330517, 'Rowokele'),
(330518, 'Sempor'),
(330519, 'Gombong'),
(330520, 'Karanganyar'),
(330521, 'Karanggayam'),
(330522, 'Sadang'),
(330523, 'Bonorowo'),
(330524, 'Padureso'),
(330525, 'Poncowarno'),
(330526, 'Karangsambung'),
(330601, 'Grabag'),
(330602, 'Ngombol'),
(330603, 'Purwodadi'),
(330604, 'Bagelen'),
(330605, 'Kaligesing'),
(330606, 'Purworejo'),
(330607, 'Banyuurip'),
(330608, 'Bayan'),
(330609, 'Kutoarjo'),
(330610, 'Butuh'),
(330611, 'Pituruh'),
(330612, 'Kemiri'),
(330613, 'Bruno'),
(330614, 'Gebang'),
(330615, 'Loano'),
(330616, 'Bener'),
(330701, 'Wadaslintang'),
(330702, 'Kepil'),
(330703, 'Sapuran'),
(330704, 'Kaliwiro'),
(330705, 'Leksono'),
(330706, 'Selomerto'),
(330707, 'Kalikajar'),
(330708, 'Kertek'),
(330709, 'Wonosobo'),
(330710, 'Watumalang'),
(330711, 'Mojotengah'),
(330712, 'Garung'),
(330713, 'Kejajar'),
(330714, 'Sukoharjo'),
(330715, 'Kalibawang'),
(330801, 'Salaman'),
(330802, 'Borobudur'),
(330803, 'Ngluwar'),
(330804, 'Salam'),
(330805, 'Srumbung'),
(330806, 'Dukun'),
(330807, 'Sawangan'),
(330808, 'Muntilan'),
(330809, 'Mungkid'),
(330810, 'Mertoyudan'),
(330811, 'Tempuran'),
(330812, 'Kajoran'),
(330813, 'Kaliangkrik'),
(330814, 'Bandongan'),
(330815, 'Candimulyo'),
(330816, 'Pakis'),
(330817, 'Ngablak'),
(330818, 'Grabag'),
(330819, 'Tegalrejo'),
(330820, 'Secang'),
(330821, 'Windusari'),
(330901, 'Selo'),
(330902, 'Ampel'),
(330903, 'Cepogo'),
(330904, 'Musuk'),
(330905, 'Boyolali'),
(330906, 'Mojosongo'),
(330907, 'Teras'),
(330908, 'Sawit'),
(330909, 'Banyudono'),
(330910, 'Sambi'),
(330911, 'Ngemplak'),
(330912, 'Nogosari'),
(330913, 'Simo'),
(330914, 'Karanggede'),
(330915, 'Klego'),
(330916, 'Andong'),
(330917, 'Kemusu'),
(330918, 'Wonosegoro'),
(330919, 'Juwangi'),
(330920, 'Gladagsari'),
(330921, 'Tamansari'),
(330922, 'Wonosamodro'),
(331001, 'Prambanan'),
(331002, 'Gantiwarno'),
(331003, 'Wedi'),
(331004, 'Bayat'),
(331005, 'Cawas'),
(331006, 'Trucuk'),
(331007, 'Kebonarum'),
(331008, 'Jogonalan'),
(331009, 'Manisrenggo'),
(331010, 'Karangnongko'),
(331011, 'Ceper'),
(331012, 'Pedan'),
(331013, 'Karangdowo'),
(331014, 'Juwiring'),
(331015, 'Wonosari'),
(331016, 'Delanggu'),
(331017, 'Polanharjo'),
(331018, 'Karanganom'),
(331019, 'Tulung'),
(331020, 'Jatinom'),
(331021, 'Kemalang'),
(331022, 'Ngawen'),
(331023, 'Kalikotes'),
(331024, 'Klaten Utara'),
(331025, 'Klaten Tengah'),
(331026, 'Klaten Selatan'),
(331101, 'Weru'),
(331102, 'Bulu'),
(331103, 'Tawangsari'),
(331104, 'Sukoharjo'),
(331105, 'Nguter'),
(331106, 'Bendosari'),
(331107, 'Polokarto'),
(331108, 'Mojolaban'),
(331109, 'Grogol'),
(331110, 'Baki'),
(331111, 'Gatak'),
(331112, 'Kartasura'),
(331201, 'Pracimantoro'),
(331202, 'Giritontro'),
(331203, 'Giriwoyo'),
(331204, 'Batuwarno'),
(331205, 'Tirtomoyo'),
(331206, 'Nguntoronadi'),
(331207, 'Baturetno'),
(331208, 'Eromoko'),
(331209, 'Wuryantoro'),
(331210, 'Manyaran'),
(331211, 'Selogiri'),
(331212, 'Wonogiri'),
(331213, 'Ngadirojo'),
(331214, 'Sidoharjo'),
(331215, 'Jatiroto'),
(331216, 'Kismantoro'),
(331217, 'Purwantoro'),
(331218, 'Bulukerto'),
(331219, 'Slogohimo'),
(331220, 'Jatisrono'),
(331221, 'Jatipurno'),
(331222, 'Girimarto'),
(331223, 'Karangtengah'),
(331224, 'Paranggupito'),
(331225, 'Puhpelem'),
(331301, 'Jatipuro'),
(331302, 'Jatiyoso'),
(331303, 'Jumapolo'),
(331304, 'Jumantono'),
(331305, 'Matesih'),
(331306, 'Tawangmangu'),
(331307, 'Ngargoyoso'),
(331308, 'Karangpandan'),
(331309, 'Karanganyar'),
(331310, 'Tasikmadu'),
(331311, 'Jaten'),
(331312, 'Colomadu'),
(331313, 'Gondangrejo'),
(331314, 'Kebakkramat'),
(331315, 'Mojogedang'),
(331316, 'Kerjo'),
(331317, 'Jenawi'),
(331401, 'Kalijambe'),
(331402, 'Plupuh'),
(331403, 'Masaran'),
(331404, 'Kedawung'),
(331405, 'Sambirejo'),
(331406, 'Gondang'),
(331407, 'Sambungmacan'),
(331408, 'Ngrampal'),
(331409, 'Karangmalang'),
(331410, 'Sragen'),
(331411, 'Sidoharjo'),
(331412, 'Tanon'),
(331413, 'Gemolong'),
(331414, 'Miri'),
(331415, 'Sumberlawang'),
(331416, 'Mondokan'),
(331417, 'Sukodono'),
(331418, 'Gesi'),
(331419, 'Tangen'),
(331420, 'Jenar'),
(331501, 'Kedungjati'),
(331502, 'Karangrayung'),
(331503, 'Penawangan'),
(331504, 'Toroh'),
(331505, 'Geyer'),
(331506, 'Pulokulon'),
(331507, 'Kradenan'),
(331508, 'Gabus'),
(331509, 'Ngaringan'),
(331510, 'Wirosari'),
(331511, 'Tawangharjo'),
(331512, 'Grobogan'),
(331513, 'Purwodadi'),
(331514, 'Brati'),
(331515, 'Klambu'),
(331516, 'Godong'),
(331517, 'Gubug'),
(331518, 'Tegowanu'),
(331519, 'Tanggungharjo'),
(331601, 'Jati'),
(331602, 'Randublatung'),
(331603, 'Kradenan'),
(331604, 'Kedungtuban'),
(331605, 'Cepu'),
(331606, 'Sambong'),
(331607, 'Jiken'),
(331608, 'Jepon'),
(331609, 'Blora'),
(331610, 'Tunjungan'),
(331611, 'Banjarejo'),
(331612, 'Ngawen'),
(331613, 'Kunduran'),
(331614, 'Todanan'),
(331615, 'Bogorejo'),
(331616, 'Japah'),
(331701, 'Sumber'),
(331702, 'Bulu'),
(331703, 'Gunem'),
(331704, 'Sale'),
(331705, 'Sarang'),
(331706, 'Sedan'),
(331707, 'Pamotan'),
(331708, 'Sulang'),
(331709, 'Kaliori'),
(331710, 'Rembang'),
(331711, 'Pancur'),
(331712, 'Kragan'),
(331713, 'Sluke'),
(331714, 'Lasem'),
(331801, 'Sukolilo'),
(331802, 'Kayen'),
(331803, 'Tambakromo'),
(331804, 'Winong'),
(331805, 'Pucakwangi'),
(331806, 'Jaken'),
(331807, 'Batangan'),
(331808, 'Juwana'),
(331809, 'Jakenan'),
(331810, 'Pati'),
(331811, 'Gabus'),
(331812, 'Margorejo'),
(331813, 'Gembong'),
(331814, 'Tlogowungu'),
(331815, 'Wedarijaksa'),
(331816, 'Margoyoso'),
(331817, 'Gunungwungkal'),
(331818, 'Cluwak'),
(331819, 'Tayu'),
(331820, 'Dukuhseti'),
(331821, 'Trangkil'),
(331901, 'Kaliwungu'),
(331902, 'Kota Kudus'),
(331903, 'Jati'),
(331904, 'Undaan'),
(331905, 'Mejobo'),
(331906, 'Jekulo'),
(331907, 'Bae'),
(331908, 'Gebog'),
(331909, 'Dawe'),
(332001, 'Kedung'),
(332002, 'Pecangaan'),
(332003, 'Welahan'),
(332004, 'Mayong'),
(332005, 'Batealit'),
(332006, 'Jepara'),
(332007, 'Mlonggo'),
(332008, 'Bangsri'),
(332009, 'Keling'),
(332010, 'Karimun Jawa'),
(332011, 'Tahunan'),
(332012, 'Nalumsari'),
(332013, 'Kalinyamatan'),
(332014, 'Kembang'),
(332015, 'Pakis Aji'),
(332016, 'Donorojo'),
(332101, 'Mranggen'),
(332102, 'Karangawen'),
(332103, 'Guntur'),
(332104, 'Sayung'),
(332105, 'Karangtengah'),
(332106, 'Wonosalam'),
(332107, 'Dempet'),
(332108, 'Gajah'),
(332109, 'Karanganyar'),
(332110, 'Mijen'),
(332111, 'Demak'),
(332112, 'Bonang'),
(332113, 'Wedung'),
(332114, 'Kebonagung'),
(332201, 'Getasan'),
(332202, 'Tengaran'),
(332203, 'Susukan'),
(332204, 'Suruh'),
(332205, 'Pabelan'),
(332206, 'Tuntang'),
(332207, 'Banyubiru'),
(332208, 'Jambu'),
(332209, 'Sumowono'),
(332210, 'Ambarawa'),
(332211, 'Bawen'),
(332212, 'Bringin'),
(332213, 'Bergas'),
(332215, 'Pringapus'),
(332216, 'Bancak'),
(332217, 'Kaliwungu'),
(332218, 'Ungaran Barat'),
(332219, 'Ungaran Timur'),
(332220, 'Bandungan'),
(332301, 'Bulu'),
(332302, 'Tembarak'),
(332303, 'Temanggung'),
(332304, 'Pringsurat'),
(332305, 'Kaloran'),
(332306, 'Kandangan'),
(332307, 'Kedu'),
(332308, 'Parakan'),
(332309, 'Ngadirejo'),
(332310, 'Jumo'),
(332311, 'Tretep'),
(332312, 'Candiroto'),
(332313, 'Kranggan'),
(332314, 'Tlogomulyo'),
(332315, 'Selopampang'),
(332316, 'Bansari'),
(332317, 'Kledung'),
(332318, 'Bejen'),
(332319, 'Wonoboyo'),
(332320, 'Gemawang'),
(332401, 'Plantungan'),
(332402, 'Pageruyung'),
(332403, 'Sukorejo'),
(332404, 'Patean'),
(332405, 'Singorojo'),
(332406, 'Limbangan'),
(332407, 'Boja'),
(332408, 'Kaliwungu'),
(332409, 'Brangsong'),
(332410, 'Pegandon'),
(332411, 'Gemuh'),
(332412, 'Weleri'),
(332413, 'Cepiring'),
(332414, 'Patebon'),
(332415, 'Kendal'),
(332416, 'Rowosari'),
(332417, 'Kangkung'),
(332418, 'Ringinarum'),
(332419, 'Ngampel'),
(332420, 'Kaliwungu Selatan'),
(332501, 'Wonotunggal'),
(332502, 'Bandar'),
(332503, 'Blado'),
(332504, 'Reban'),
(332505, 'Bawang'),
(332506, 'Tersono'),
(332507, 'Gringsing'),
(332508, 'Limpung'),
(332509, 'Subah'),
(332510, 'Tulis'),
(332511, 'Batang'),
(332512, 'Warungasem'),
(332513, 'Kandeman'),
(332514, 'Pecalungan'),
(332515, 'Banyuputih'),
(332601, 'Kandangserang'),
(332602, 'Paninggaran'),
(332603, 'Lebakbarang'),
(332604, 'Petungkriyono'),
(332605, 'Talun'),
(332606, 'Doro'),
(332607, 'Karanganyar'),
(332608, 'Kajen'),
(332609, 'Kesesi'),
(332610, 'Sragi'),
(332611, 'Bojong'),
(332612, 'Wonopringgo'),
(332613, 'Kedungwuni'),
(332614, 'Buaran'),
(332615, 'Tirto'),
(332616, 'Wiradesa'),
(332617, 'Siwalan'),
(332618, 'Karangdadap'),
(332619, 'Wonokerto'),
(332701, 'Moga'),
(332702, 'Pulosari'),
(332703, 'Belik'),
(332704, 'Watukumpul'),
(332705, 'Bodeh'),
(332706, 'Bantarbolang'),
(332707, 'Randudongkal'),
(332708, 'Pemalang'),
(332709, 'Taman'),
(332710, 'Petarukan'),
(332711, 'Ampelgading'),
(332712, 'Comal'),
(332713, 'Ulujami'),
(332714, 'Warungpring'),
(332801, 'Margasari'),
(332802, 'Bumijawa'),
(332803, 'Bojong'),
(332804, 'Balapulang'),
(332805, 'Pagerbarang'),
(332806, 'Lebaksiu'),
(332807, 'Jatinegara'),
(332808, 'Kedungbanteng'),
(332809, 'Pangkah'),
(332810, 'Slawi'),
(332811, 'Adiwerna'),
(332812, 'Talang'),
(332813, 'Dukuhturi'),
(332814, 'Tarub'),
(332815, 'Kramat'),
(332816, 'Suradadi'),
(332817, 'Warureja'),
(332818, 'Dukuhwaru'),
(332901, 'Salem'),
(332902, 'Bantarkawung'),
(332903, 'Bumiayu'),
(332904, 'Paguyangan'),
(332905, 'Sirampog'),
(332906, 'Tonjong'),
(332907, 'Jatibarang'),
(332908, 'Wanasari'),
(332909, 'Brebes'),
(332910, 'Songgom'),
(332911, 'Kersana'),
(332912, 'Losari'),
(332913, 'Tanjung'),
(332914, 'Bulakamba'),
(332915, 'Larangan'),
(332916, 'Ketanggungan'),
(332917, 'Banjarharjo'),
(337101, 'Magelang Selatan'),
(337102, 'Magelang Utara'),
(337103, 'Magelang Tengah'),
(337201, 'Laweyan'),
(337202, 'Serengan'),
(337203, 'Pasar Kliwon'),
(337204, 'Jebres'),
(337205, 'Banjarsari'),
(337301, 'Sidorejo'),
(337302, 'Tingkir'),
(337303, 'Argomulyo'),
(337304, 'Sidomukti'),
(337401, 'Semarang Tengah'),
(337402, 'Semarang Utara'),
(337403, 'Semarang Timur'),
(337404, 'Gayamsari'),
(337405, 'Genuk'),
(337406, 'Pedurungan'),
(337407, 'Semarang Selatan'),
(337408, 'Candisari'),
(337409, 'Gajahmungkur'),
(337410, 'Tembalang'),
(337411, 'Banyumanik'),
(337412, 'Gunungpati'),
(337413, 'Semarang Barat'),
(337414, 'Mijen'),
(337415, 'Ngaliyan'),
(337416, 'Tugu'),
(337501, 'Pekalongan Barat'),
(337502, 'Pekalongan Timur'),
(337503, 'Pekalongan Utara'),
(337504, 'Pekalongan Selatan'),
(337601, 'Tegal Barat'),
(337602, 'Tegal Timur'),
(337603, 'Tegal Selatan'),
(337604, 'Margadana');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daerah`
--

CREATE TABLE `tbl_daerah` (
  `kode_daerah` int(4) NOT NULL,
  `nama_daerah` varchar(255) NOT NULL,
  `no_daerah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tbl_daerah`
--

INSERT INTO `tbl_daerah` (`kode_daerah`, `nama_daerah`, `no_daerah`) VALUES
(3301, 'Kabupaten Cilacap', 1),
(3302, 'Kabupaten Banyumas', 2),
(3303, 'Kabupaten Purbalingga', 3),
(3304, 'Kabupaten Banjarnegara', 4),
(3305, 'Kabupaten Kebumen', 5),
(3306, 'Kabupaten Purworejo', 6),
(3307, 'Kabupaten Wonosobo', 7),
(3308, 'Kabupaten Magelang', 8),
(3309, 'Kabupaten Boyolali', 9),
(3310, 'Kabupaten Klaten', 10),
(3311, 'Kabupaten Sukoharjo', 11),
(3312, 'Kabupaten Wonogiri', 12),
(3313, 'Kabupaten Karanganyar', 13),
(3314, 'Kabupaten Sragen', 14),
(3315, 'Kabupaten Grobogan', 15),
(3316, 'Kabupaten Blora', 16),
(3317, 'Kabupaten Rembang', 17),
(3318, 'Kabupaten Pati', 18),
(3319, 'Kabupaten Kudus', 19),
(3320, 'Kabupaten Jepara', 20),
(3321, 'Kabupaten Demak', 21),
(3322, 'Kabupaten Semarang', 22),
(3323, 'Kabupaten Temanggung', 23),
(3324, 'Kabupaten Kendal', 24),
(3325, 'Kabupaten Batang', 25),
(3326, 'Kabupaten Pekalongan', 26),
(3327, 'Kabupaten Pemalang', 27),
(3328, 'Kabupaten Tegal', 28),
(3329, 'Kabupaten Brebes', 29),
(3371, 'Kota Magelang', 30),
(3372, 'Kota Surakarta', 31),
(3373, 'Kota Salatiga', 32),
(3374, 'Kota Semarang', 33),
(3375, 'Kota Pekalongan', 34),
(3376, 'Kota Tegal', 35);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level`
--

CREATE TABLE `tbl_level` (
  `id` int(3) NOT NULL,
  `nama_level` varchar(100) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_level`
--

INSERT INTO `tbl_level` (`id`, `nama_level`, `keterangan`) VALUES
(1, 'root', ''),
(2, 'Panitia Musywil', ''),
(3, 'Pimpinan Wilayah', ''),
(4, 'Pimpinan Daerah', ''),
(5, 'Peserta Musywil', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_progres_report`
--

CREATE TABLE `tbl_progres_report` (
  `id` int(11) NOT NULL,
  `keterangan_file` varchar(255) NOT NULL,
  `daerah` int(4) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `file_size` varchar(30) NOT NULL,
  `users_id` int(11) NOT NULL,
  `tgl_unggah` datetime DEFAULT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '0',
  `tgl_validasi` datetime DEFAULT NULL,
  `user_validasi` int(11) DEFAULT NULL,
  `keterangan_validasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_mandat`
--

CREATE TABLE `tbl_surat_mandat` (
  `id` int(11) NOT NULL,
  `keterangan_file` varchar(255) NOT NULL,
  `daerah` int(4) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `file_size` varchar(30) NOT NULL,
  `users_id` int(11) NOT NULL,
  `tgl_unggah` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '0',
  `tgl_validasi` datetime DEFAULT NULL,
  `user_validasi` int(11) DEFAULT NULL,
  `keterangan_validasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verifikasi_pendaftar`
--

CREATE TABLE `tbl_verifikasi_pendaftar` (
  `id` int(11) NOT NULL,
  `id_pendaftar` int(11) NOT NULL,
  `verifikasi` enum('1','2') DEFAULT NULL,
  `tgl_verifikasi` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `foto_ktp` varchar(100) DEFAULT NULL,
  `foto_nbm` varchar(100) DEFAULT NULL,
  `pas_foto` varchar(100) DEFAULT NULL,
  `surat_mandat` varchar(100) DEFAULT NULL,
  `sk_pimpinan` varchar(100) DEFAULT NULL,
  `swo` varchar(100) DEFAULT NULL,
  `swp` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `pendaftar_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL,
  `daerah` int(4) NOT NULL,
  `level_user` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `password`, `level`, `daerah`, `level_user`) VALUES
(1, 'YD2SMG', 'kuniwa', '$2y$09$LAfXzr/X9lR847t3TTRXHO3Z58XZE3N/n5JgPJ81Iidw37b1lJlUe', 'admin', 3374, 1),
(2, 'Muzain', 'zain', '$2y$09$r3zsrlOHlziAx4S2v2X48OwIYgGEzoXmYiROgsX1LR3OIk.prHwnG', 'admin', 3374, 2),
(3, 'Muzain2', 'zain2', '$2y$09$fL6KJ7jX0VhFdwxMcZ2FYuk8rOKOj791jitkrpsZ9mufyM5j44g6S', 'admin', 3374, 2),
(4, 'Muzain3', 'zain3', '$2y$09$uGhmci7nXmJj2FzqDymukOq2QVhJ47i1PQnqsYWF3BmDZljTwy.B2', 'admin', 3374, 2),
(5, 'Muzain4', 'zain4', '$2y$09$7T8BETlAq1JMK4IsrkLmH.ol7kdHGNwaZBuyNgqL4LrRWT2uNdz6C', 'admin', 3374, 2),
(6, 'Muzain5', 'zain5', '$2y$09$I8tDYjxq2/OjuYD83/vCpuNw2XInu.wQDh7bCvZ4ZsprKznFgXlRi', 'admin', 3374, 2),
(7, 'Kabupaten Cilacap', '3301', '$2y$09$ssEF1XNWOg3gVqhR/c8vgOu.Nw9v5WZNgIod1uQhngujk9YKi4V4W', 'peserta', 3301, 4),
(8, 'Kabupaten Banyumas', '3302', '$2y$09$KQd6wUed8sWke1l85ksly.PTYKaW72aaToHdR2VrWR.kbaN98X6jO', 'peserta', 3302, 4),
(9, 'Kabupaten Purbalingga', '3303', '$2y$09$VnPnitm7XBPqjzapXAe2F.q5KqwkWLf9K01gQQUoDqIMfZJiCEiPq', 'peserta', 3303, 4),
(10, 'Kabupaten Banjarnegara', '3304', '$2y$09$trLsb2KtMpQcAkTMsiQ0Ke0/3NWvBNw7GBVUCSnAS5GBkkrqnxwD2', 'peserta', 3304, 4),
(11, 'Kabupaten Kebumen', '3305', '$2y$09$BhR4rMcsoCoXX9RvZLoPouCZexANXZqeSqGxctFUmfnubIYxFrKjW', 'peserta', 3305, 4),
(12, 'Kabupaten Purworejo', '3306', '$2y$09$5MMS6KPumm/CsHtbGiCP3upNJ9288RjDrZotzExrMLYGTdxvP01sa', 'peserta', 3306, 4),
(13, 'Kabupaten Wonosobo', '3307', '$2y$09$RogfJ8kMbEcGIXxlFBBV9OUutTGD3AxWV7roQmW0KskloHNj.Eh5K', 'peserta', 3307, 4),
(14, 'Kabupaten Magelang', '3308', '$2y$09$zcy7PvEjaN.2WqAbJD/ejO4ow98DbdomlWQ7O1bdstRKX9G37PB6G', 'peserta', 3308, 4),
(15, 'Kabupaten Boyolali', '3309', '$2y$09$b5xhaoYdiune4gfL18SKE.2eeunCydvRVjuU4oDQsB2HyJMsIz9O2', 'peserta', 3309, 4),
(16, 'Kabupaten Klaten', '3310', '$2y$09$W8CL3bJhVsrNn.ZMPtSgp.BrQNSdVEqJII91hraHkGcVzOLYr9Xza', 'peserta', 3310, 4),
(17, 'Kabupaten Sukoharjo', '3311', '$2y$09$h9yVpIjIAUkqJceXeJ1MsO8ZzcvLO7SyEq5JxXW7nby8FOyzCZAaS', 'peserta', 3311, 4),
(18, 'Kabupaten Wonogiri', '3312', '$2y$09$PYi6GWiJ.a8nFyTISNuMPuGh.3N.SOiHjAN1pbNv.ZPAQphK2/bf6', 'peserta', 3312, 4),
(19, 'Kabupaten Karanganyar', '3313', '$2y$09$lJuYFAJ4NiZHE/yBKITNBuNJJ3ZwRa3OI4pUUJdUrFDO9fro5RwkK', 'peserta', 3313, 4),
(20, 'Kabupaten Sragen', '3314', '$2y$09$rDaA7FltXaWA02q6FZXfTu9a1IOSQXQPAzn4PHXDcBmktPiAlf652', 'peserta', 3314, 4),
(21, 'Kabupaten Grobogan', '3315', '$2y$09$Zw/pEkKwVpExrwT406t4dult.2SXxpkl1JRu3z5IOlGAYIM4Zv8Ey', 'peserta', 3315, 4),
(22, 'Kabupaten Blora', '3316', '$2y$09$YKceNTbCS/FX6QRMQEOgf.gNiOCw.hKZmn/EYOhYxjik9FnHsEyFm', 'peserta', 3316, 4),
(23, 'Kabupaten Rembang', '3317', '$2y$09$xyaXVf/4XYtXlwzJRJnH6.fZ.M4njy4t64em5AuQAGVTrMCAYi2pq', 'peserta', 3317, 4),
(24, 'Kabupaten Pati', '3318', '$2y$09$w6UoinrdLhZ9xY6aQyFxZuVJ5lknL2MiWnCeu9hF1C6/3fGan6bSu', 'peserta', 3318, 4),
(25, 'Kabupaten Kudus', '3319', '$2y$09$0bVjwFfy1I3b5VXdc.WwvOsZwJVdcW5qJLlWD2WBcv5H3c/WUZAc6', 'peserta', 3319, 4),
(26, 'Kabupaten Jepara', '3320', '$2y$09$LxUVb98Z6n0H6OuE1aBJc.Qq9aUmO/M1nPRnmbi1opsHp0W0FetM2', 'peserta', 3320, 4),
(27, 'Kabupaten Demak', '3321', '$2y$09$V3yjObQYmeC3z1ui0ZZsl.aL0r10zE24U/BmYIWCMaO5ZTzMiYSDC', 'peserta', 3321, 4),
(28, 'Kabupaten Semarang', '3322', '$2y$09$43b1bvfubFryPm3ezksllO4fNKdP3ICrW6.UzlJO5NfRZNuka6IBK', 'peserta', 3322, 4),
(29, 'Kabupaten Temanggung', '3323', '$2y$09$mCIkR4zkrbq7AH8aytxRHe8FxhNYL8BfWAhkli33rB0KBYUBl8qUS', 'peserta', 3323, 4),
(30, 'Kabupaten Kendal', '3324', '$2y$09$J2qJ4zDpK6Y58z8crzlIeuylMQWnaluyQqTOtO0TAAz9aUynUK5ha', 'peserta', 3324, 4),
(31, 'Kabupaten Batang', '3325', '$2y$09$lfn/eoHFhCzsV4fip0WIr.CfXZj3Bl4SehvS7Z6l9tmwYyG5pb4Ty', 'peserta', 3325, 4),
(32, 'Kabupaten Pekalongan', '3326', '$2y$09$LAW8an0NdIuDSP4AMSSqJunjsZfFomuPxOKMc0vIEo23LxH7toAI.', 'peserta', 3326, 4),
(33, 'Kabupaten Pemalang', '3327', '$2y$09$sphJ1RaKlBiiIJ3GzXNLRufpRswGyssjbNiV9YnAlG8ot9SiudLau', 'peserta', 3327, 4),
(34, 'Kabupaten Tegal', '3328', '$2y$09$9RkusItHuUSxjhT28RsVlO52WfWlcsoXilEW/XRX6R3jh9Ht8Dd3i', 'peserta', 3328, 4),
(35, 'Kabupaten Brebes', '3329', '$2y$09$DS.UXs8Lg/j9fyzII5q1yOLs2tb3h9QDI3E6PrT5and5pNhHtSsf.', 'peserta', 3329, 4),
(36, 'Kota Magelang', '3371', '$2y$09$H.OwxdsxPV.g2TdZpkjeseDJXt84zVJqKm7gzYQHyU9zAvPLZD/Jq', 'peserta', 3371, 4),
(37, 'Kota Surakarta', '3372', '$2y$09$hWzGqu63hZF3ekzDu6sOo.VT2Mm.TfH444/r7tp9XgiGC5nHJQ2Gi', 'peserta', 3372, 4),
(38, 'Kota Salatiga', '3373', '$2y$09$q.jmXoIT9XMlKsPC6lGCYOhO7rf2kXpwCg4ehy7wD97rQUkkv52ey', 'peserta', 3373, 4),
(39, 'Kota Semarang', '3374', '$2y$09$IneL5Sw31Vd0rfYyISoZousgDd6mA9D/jWjhHQDwpH.GOMox3FX.u', 'peserta', 3374, 4),
(40, 'Kota Pekalongan', '3375', '$2y$09$99taYtyHEP33glUgBC2VUeCB93lpa8LNyk7GBIDCUAgdD1PJb8.2W', 'peserta', 3375, 4),
(41, 'Kota Tegal', '3376', '$2y$09$9D7PQetK4ylDZe9LcWjbYOAPvde9hVadGb6jVSHZYmORsoSRpV.4.', 'peserta', 3376, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catatan`
--
ALTER TABLE `catatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daerah_selesai_verifikasi`
--
ALTER TABLE `daerah_selesai_verifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kirim`
--
ALTER TABLE `data_kirim`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_daerah` (`daerah`);

--
-- Indexes for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nbm` (`nbm`) USING BTREE,
  ADD UNIQUE KEY `wa` (`nomor`),
  ADD KEY `fk_users_id` (`users_id`);

--
-- Indexes for table `tbl_bukti_bayar`
--
ALTER TABLE `tbl_bukti_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cabang`
--
ALTER TABLE `tbl_cabang`
  ADD PRIMARY KEY (`kode_cabang`) USING BTREE,
  ADD UNIQUE KEY `unix` (`kode_cabang`) USING BTREE;

--
-- Indexes for table `tbl_daerah`
--
ALTER TABLE `tbl_daerah`
  ADD PRIMARY KEY (`kode_daerah`) USING BTREE,
  ADD UNIQUE KEY `unix` (`kode_daerah`) USING BTREE;

--
-- Indexes for table `tbl_level`
--
ALTER TABLE `tbl_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_progres_report`
--
ALTER TABLE `tbl_progres_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_surat_mandat`
--
ALTER TABLE `tbl_surat_mandat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_verifikasi_pendaftar`
--
ALTER TABLE `tbl_verifikasi_pendaftar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_nilai_pendaftar1_idx` (`pendaftar_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `username_UNIQUE` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catatan`
--
ALTER TABLE `catatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `daerah_selesai_verifikasi`
--
ALTER TABLE `daerah_selesai_verifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_kirim`
--
ALTER TABLE `data_kirim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pendaftar`
--
ALTER TABLE `pendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_bukti_bayar`
--
ALTER TABLE `tbl_bukti_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_level`
--
ALTER TABLE `tbl_level`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_progres_report`
--
ALTER TABLE `tbl_progres_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_surat_mandat`
--
ALTER TABLE `tbl_surat_mandat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_verifikasi_pendaftar`
--
ALTER TABLE `tbl_verifikasi_pendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
