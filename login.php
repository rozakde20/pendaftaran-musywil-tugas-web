<?php session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aplikasi Pendaftaran Musywil Pemuda Muhammadiyah Jateng">
    <meta name="author" content="muhammad-muza'in">

    <title>Login - Musywil PWM Jateng</title>
    <link rel="icon" href="assets/img/logo-pm.png" type="image/png">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
     <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Style untuk mengatur logo -->
    <style>
        .img-logo {
            max-height: 160px;
            margin-bottom: 20px;
        }
    </style>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-md-7">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img src="assets/img/logo-pm.png" alt="logo-aplikasi" class="img-logo">
                                        <h1 class="h4 text-gray-900 font-weight-bold">Masuk Musyawarah Wilayah</h1>
                                        <h1 class="h4 text-gray-900 mb-4 font-weight-bold">Pemuda Muhammadiyah Jawa Tengah</h1>
                                        <?php 
                                        
                                        
                                        if(isset($_SESSION['pesan_registrasi'])) { ?>

                                        <div class="alert alert-success">
                                        <?= $_SESSION['pesan_registrasi'] ?>
                                        </div>
                    
                                        <?php } 
                                        
                                        if(isset($_SESSION['login_error'])) { ?>

                                        <div class="alert alert-danger">
                                        <?= $_SESSION['login_error'] ?>
                                        
                                        </div>

                                        <?php } 
                                        
                                        session_destroy();
                                        ?>

                                    </div>
                                    <form class="user" action="login_control.php" method="post">
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="email">No. Whatsapp/username</label>
                                            <input type="text" name="email" id="email"  class="form-control form-control-user text-gray-900"
                                                id="email"
                                                placeholder="Masukkan nomor anda">
                                        </div>
                                        <div class="form-group text-gray-900 font-weight-bold">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" class="form-control form-control-user text-gray-900"
                                                id="password" placeholder="Password">
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="6LevyhknAAAAAJzSj8jINUemYoZzcXiTbZ7JjvMV"></div>
                                        <br/>
                                        <button class="btn btn-primary btn-user btn-block" name="btn_login" value="login"  >Login</button>
                    
                                    </form>
                                    <!--<hr>-->
                                    <!--<div class="text-center">-->
                                    <!--    <a class="small" href="register">Daftar Peserta Musywil</a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>