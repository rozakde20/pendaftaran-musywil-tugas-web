<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Musyawarah Wilayah Pemuda Muhammadiyah Jawa Tengah">
    <meta name="author" content="PWPM Jateng">

    <title>Peserta</title>

    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../assets/css/css_styles.css" rel="stylesheet">
    <link href="../assets/css/styles_alert.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon">
                    <i class="fas fa-id-card"></i>
                </div>
                <div class="sidebar-brand-text mx-3"><?=strtoupper($_SESSION['level'])?></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Heading -->
            <div class="sidebar-heading">
            <?= $_SESSION['level'] ?>
            </div>
 
            <?php if($_SESSION['level'] == 'peserta') { ?> 

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Nav Item - Formulir -->
            <!--<li class="nav-item">-->
            <!--    <a class="nav-link" href="data-diri.php">-->
            <!--        <i class="fas fa-fw fa-file"></i>-->
            <!--        <span>Isi Formulir</span></a>-->
            <!--</li>-->
            <!-- Nav Item - Import Excel -->
            <li class="nav-item">
                <a class="nav-link" href="daftar_kolektif">
                    <i class="fas fa-fw fa-file"></i>
                    <span>Daftar Kolektif</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="unggah_bukti_bayar">
                    <i class="fas fa-fw fa-upload"></i>
                    <span>Unggah Bukti Bayar</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="surat_mandat">
                    <i class="fas fa-fw fa-upload"></i>
                    <span>Unggah Surat Mandat</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="progres_report">
                    <i class="fas fa-fw fa-upload"></i>
                    <span>Unggah Progres Report</span></a>
            </li>

            <!-- Nav Item - Pembayaran -->
            <!-- <li class="nav-item">
                <a class="nav-link" href="pembayaran.php">
                    <i class="fas fa-fw fa-credit-card"></i>
                    <span>Pembayaran</span></a>
            </li> -->
            <?php } ?>

            <?php if($_SESSION['level'] == 'admin') { ?> 
            <li class="nav-item">
            <a class="nav-link" href="dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
            </li>

            <!-- Nav Item - data Pendaftaran -->
            <!--<li class="nav-item">-->
            <!--    <a class="nav-link" href="pendaftaran">-->
            <!--        <i class="fas fa-fw fa-tachometer-alt"></i>-->
            <!--        <span>Data Pendaftaran</span></a>-->
            <!--</li>-->

            <!-- Nav Item - Laporan -->
            <!--<li class="nav-item">-->
            <!--    <a class="nav-link" href="">-->
            <!--        <i class="fas fa-fw fa-tachometer-alt"></i>-->
            <!--        <span>Laporan</span></a>-->
            <!--</li>-->
            
            <?php } ?>
            <?php if($_SESSION['level'] == 'utusan') { ?> 
            <li class="nav-item">
            <a class="nav-link" href="dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-paperclip"></i>
                    <span>Materi Musywil</span></a>
            </li>

            <!--Nav Item - Laporan -->
            <li class="nav-item">
                <a class="nav-link" href="">
                    <i class="fas fa-fw fa-calendar"></i>
                    <span>Jadwal</span></a>
            </li>
            
            <?php } ?>
            <li class="nav-item">
                <a class="nav-link" href="../logout" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Keluar</span></a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                    


                            </a>


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['nama']; ?></span>
                                <img class="img-profile rounded-circle"
                                    src="../assets/img/avatar.png">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                            
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?='../logout';?>" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Keluar
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->