<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Data Pendaftar</h1>
                    
                    <hr class="mt-3">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>NBM</td>
                                    <td>Jabatan</td>
                                    <td>Daerah</td>
                                    <td>Cabang</td>
                                    <td>Status</td>
                                    <td>Option</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Dr. Muhammad Muza'in, S.Kom, M.Kom</td>
                                    <td>331675554</td>
                                    <td>Ketua Bidang Komunikasi</td>
                                    <td>Semarang</td>
                                    <td>Tembalang</td>
                                    <td class="badge badge-success">Terverifikasi</td>
                                    <td><a href="detail-pendaftar.php" class="btn btn-primary btn-sm">Cek</a>
                                        <a href="" class="btn btn-warning btn-sm">Print Pdf</a>
                                        <a href="" class="btn btn-danger btn-sm">Hapus</a>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>