<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Data Pendaftar</h1>
                    <hr class="mt-3">
                    <div class="row">
                    <div class="col-md-8">
                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Profil</h6>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <div class="alert alert-warning">
                                            Data Pendaftar Belum Terverifikasi 
                                        </div>
                                        <img src="../assets/img/avatar.png" alt="foto-profil" class="img-fluid rounded-circle" style="width: 200px">
                                        <ul class="list-group">
                                        <li class="list-group-item">
                                        <button type="button" class="btn btn-primary text-left"
                                                data-toggle="modal" data-target="#modal-validasi">
                                                Validasi
                                            </button>
                                        <button type="button" class="btn btn-danger text-left"
                                                data-toggle="modal" data-target="#modal-hapus">
                                                Hapus
                                            </button>
                                        </li>
                                    </ul>
                                    
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <h6>Nama</h6>
                                                <small>Muhammad Muza'in</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Tempat, Tanggal Lahir</h6>
                                                <small>Banyumas, 20 Februari 1991</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>NIK</h6>
                                                <small>323444499393</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Jabatan</h6>
                                                <small>Sekretaris</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>NBM</h6>
                                                <small>12123444544</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Asal daerah</h6>
                                                <small>Banyumas</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Asal Cabang</h6>
                                                <small>Banyuurip</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>No. Whatsapp</h6>
                                                <small>08223334444</small>
                                            </li>
                                            
                                            <li class="list-group-item">
                                                <h6>Email</h6>
                                                <small>peserta@gmail.com</small>
                                            </li>
                                            <li class="list-group-item">
                                                <h6>Ukuran Kaos</h6>
                                                <small>L</small>
                                            </li>
                                            <li class="list-group-item">
                                            <button type="button" class="btn btn-info btn-block"
                                                data-toggle="modal" data-target="#modal-ktp">
                                                Foto KTP
                                            </button>
                                            </li>
                                            <li class="list-group-item">
                                            <button type="button" class="btn btn-info btn-block"
                                                data-toggle="modal" data-target="#modal-nbm">
                                                Foto NBM
                                            </button>
                                            </li>
                                            <li class="list-group-item">
                                            <button type="button" class="btn btn-info btn-block"
                                                data-toggle="modal" data-target="#modal-pas">
                                                Pas Foto
                                            </button>
                                            </li>
                                            <li class="list-group-item">
                                            <button type="button" class="btn btn-info btn-block"
                                                data-toggle="modal" data-target="#modal-mandat">
                                                Surat Mandat
                                            </button>
                                            </li>
                                            <li class="list-group-item">
                                            <button type="button" class="btn btn-info btn-block"
                                                data-toggle="modal" data-target="#modal-sk">
                                                SK Pimpinan
                                            </button>
                                            </li>
                                        </ul>

                                        
    
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                    
                    
                            <!-- BUKTI PEMBAYARN -->
                                <div class="col-md-4">
                                    <div class="card-shadow mb-4">
                                        <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Bukti Pembayaran</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="text-center">
                                                <ul class="list-group">
                                                <li class="list-group-item">
                                                    <button type="button" class="btn btn-info btn-block"
                                                        data-toggle="modal" data-target="#modal-swo">
                                                        Sumbangan Wajib Organisasi
                                                    </button>
                                                    </li>
                                                <li class="list-group-item">
                                                    <button type="button" class="btn btn-info btn-block"
                                                        data-toggle="modal" data-target="#modal-swp">
                                                        Sumbangan Wajib Peserta
                                                    </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <!-- -----------------MODAL DIALOG UNTUK MEMUNCULKAN POP UP DATA FOTO -------------- -->

                            <!-- MODAL FOTO KTP -->

                                <div class="modal fade" id="modal-ktp" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Foto KTP
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <!-- MODAL NBM -->

                                <div class="modal fade" id="modal-nbm" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    foto NBM
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <!-- MODAL PAS FOTO -->

                                <div class="modal fade" id="modal-pas" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Pas Foto
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- MODAL PAS FOTO -->

                                <div class="modal fade" id="modal-mandat" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Surat Mandat
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- MODAL PAS FOTO -->

                                <div class="modal fade" id="modal-sk" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    SK Pimpinan
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- MODAL PAS SWO -->

                                <div class="modal fade" id="modal-swo" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Bukti Pembayaran SWO
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- MODAL PAS SWP -->

                                <div class="modal fade" id="modal-swp" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <!-- Modal heading -->
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Bukti Pembayaran SWP
                                                </h5>
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">
                                                        ×
                                                    </span>
                                                </button>
                                            </div>
                                            <!-- Modal body with image -->
                                            <div class="modal-body">
                                                <div class="card-body">

                                                    <img src="../assets/img/avatar.png" class="img-fluid"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- MODAL VALIDASI -->
                                <div class="modal fade" id="modal-validasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Validasi</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Apakah anda yakin ingin memvalidasi peserta?</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
                                                <a class="btn btn-primary" href="../login.php">Validasi</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- MODAL HAPUS PESERTA -->
                                <div class="modal fade" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Validasi</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Apakah anda yakin ingin Menghapus peserta?</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
                                                <a class="btn btn-primary" href="../login.php">Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div> 
                    <!-- DIV ROW -->

                    
                                    
                    
                </div>
                <!-- /.container-fluid -->
<?php include('../template/footer.php') ?>