<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <!--<h1 class="h3 mb-4 text-gray-800">Data Utusan</h1>-->
                    
                    <?php
                    if(isset($_SESSION['flash_message_success'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-check-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Success</h6>
                                    <?=$_SESSION['flash_message_success'];unset($_SESSION['flash_message_success']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                    if(isset($_SESSION['flash_message_error'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-times-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Failed</h6>
                                    <?=$_SESSION['flash_message_error'];unset($_SESSION['flash_message_error']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--<hr class="mt-3">-->
                    <h1 class="h3 mb-0 text-gray-800 table-responsive">Halaman Verifikasi Berkas Daerah <?=nama_daerah(id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))).' '.id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))?>
                    <a href="dashboard" class="btn btn-sm btn-danger" style="margin-bottom:15px;"><i class="fas fa-flag"></i>  Selesai Verifikasi Berkas</a> <a href="dashboard"  style="margin-bottom:15px;" class="btn btn-sm btn-primary">Back</a>               
                    </h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                    <h1 class="h3 mb-0 text-gray-800 table-responsive">Berkas Bukti Bayar Daerah <?=nama_daerah(id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))).' '.id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))?></h1>
                                    
                                    </div>
                                </div>
                                <div class="card-header table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No.</td>
                                            <td>Keterangan</td>
                                            <td>Tanggal Unggah</td>
                                            <td>File Bukti Bayar</td>
                                            <td>Status Validasi</td>
                                            <td>Tanggal Validasi</td>
                                            <td>option</td>
                                        </tr>
                                        <?php
                                $sql_user = "SELECT * FROM tbl_bukti_bayar where daerah='".id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))."' order by id desc";
                                $result_user = mysqli_query($koneksi, $sql_user);
                                if(mysqli_num_rows($result_user)> 0) {
                                    $no=1;
                                    while($data_user = mysqli_fetch_array($result_user)){
                                        echo "<tr>
                                        <td>".$no.".</td>
                                        <td>".$data_user['keterangan_file']."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_unggah'])."</td>
                                        <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#buktibayar".$data_user['id']."\">Lihat Foto</a> #".$data_user['id']."</td>
                                        <td>".($data_user['is_valid']=="1"?"<span style='color:green'>Terverifikasi</span>":"<span style='color:orange'>Waiting</span>")."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_validasi'],'-')."</td>
                                        <td> ".($data_user['is_valid']=="1"?"<a class='btn btn-danger btn-sm' data-toggle=\"modal\" data-target=\"#batalkan_verifikasi_bb_".$data_user['id']."\">Batalkan Verifikasi</a>":"<a class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#verifikasi_bb_".$data_user['id']."\">Verifikasi</a>")."</td>
                                        </tr>";
                                        //Modal Foto
                                        $html_foto="<image src='../".$data_user['path']."/".$data_user['file_name']."' width='450'><br/>".$data_user['nama'];
                                        echo modal('buktibayar'.$data_user['id'],'Bukti Bayar',$html_foto);
                                        
                                        //Modal verifikasi_pr_
                                        $html_vbb="Apakah Anda yakin Akan memverifikasi berkas ini (No.".$no.")?";
                                        $btn_vbb[$data_user['id']]='<a class="btn btn-danger" href="ver-'._smgenc($data_user['id'].'#1#1#'._smgdec($_GET['id'])).'" >Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>';
                                        echo modal('verifikasi_bb_'.$data_user['id'],'Verifikasi Bukti Bayar #'.$data_user['id'],$html_vbb,'',$btn_vbb[$data_user['id']]);
                                        
                                        //Modal batalkan_verifikasi_bb_
                                        $html_bvbb="Apakah Anda yakin Akan membatalkan verifikasi berkas ini (No.".$no.")?";
                                        $btn_bvbb[$data_user['id']]='<a class="btn btn-danger" href="ver-'._smgenc($data_user['id'].'#1#0#'._smgdec($_GET['id'])).'" >Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>';
                                        echo modal('batalkan_verifikasi_bb_'.$data_user['id'],'Pembatalan Verifikasi Bukti Bayar #'.$data_user['id'],$html_bvbb,'',$btn_bvbb[$data_user['id']]);
                                        
                                        $no++;
                                    }
                                }
                                else{ 
                                ?>
                                        <tr>
                                            <td colspan="9">- empty -</td>
                                        </tr>
                                <?php
                                }
                                ?>
                                       
                                        
                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                    <h1 class="h3 mb-0 text-gray-800">Berkas Progres Report Organisasi - Daerah <?=nama_daerah(id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))).' '.id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))?></h1>
                                    
                                    </div>
                                </div>
                                <div class="card-header table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No.</td>
                                            <td>Keterangan</td>
                                            <td>Tanggal Unggah</td>
                                            <td>File Progres Report Org</td>
                                            <td>Status Validasi</td>
                                            <td>Tanggal Validasi</td>
                                            <td>option</td>
                                        </tr>
                                        <?php
                                $sql_user = "SELECT * FROM tbl_progres_report where daerah='".id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))."' order by id desc";
                                $result_user = mysqli_query($koneksi, $sql_user);
                                if(mysqli_num_rows($result_user)> 0) { 
                                    $no=1;
                                    while($data_user = mysqli_fetch_array($result_user)){
                                        echo "<tr>
                                        <td>".$no.".</td>
                                        <td>".$data_user['keterangan_file']."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_unggah'])."</td>
                                        <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#file".$data_user['id']."\">Lihat File </a> #".$data_user['id']."</td>
                                        <td>".($data_user['is_valid']=="1"?"<span style='color:green'>Terverifikasi</span>":"<span style='color:orange'>Waiting</span>")."</td>
                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_validasi'],'-')."</td>
                                        <td> ".($data_user['is_valid']=="1"?"<a class='btn btn-danger btn-sm' data-toggle=\"modal\" data-target=\"#batalkan_verifikasi_pr_".$data_user['id']."\">Batalkan Verifikasi</a>":"<a class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#verifikasi_pr_".$data_user['id']."\">Verifikasi</a>")."</td>
                                        </tr>";
                                        //Modal Foto
                                        $html_foto="<object data='../".$data_user['path']."/".$data_user['file_name']."' width='450' height='500'></object>";
                                        echo modal('file'.$data_user['id'],'Progres Report',$html_foto);
                                        
                                        //Modal verifikasi_pr_
                                        $html_vpr="Apakah Anda yakin Akan memverifikasi berkas ini (No.".$no.")?";
                                        $btn_[$data_user['id']]='<a class="btn btn-danger" href="ver-'._smgenc($data_user['id'].'#2#1#'._smgdec($_GET['id'])).'">Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>';
                                        echo modal('verifikasi_pr_'.$data_user['id'],'Verifikasi Progres Report Organisasi #'.$data_user['id'],$html_vpr,'',$btn_[$data_user['id']]);
                                        
                                        //Modal batalkan_verifikasi_bb_
                                        $html_bvpr="Apakah Anda yakin Akan membatalkan verifikasi berkas ini (No.".$no.")?";
                                        $btn_bvpr[$data_user['id']]='<a class="btn btn-danger" href="ver-'._smgenc($data_user['id'].'#2#0#'._smgdec($_GET['id'])).'" >Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>';
                                        echo modal('batalkan_verifikasi_pr_'.$data_user['id'],'Pembatalan Verifikasi Progres Report Organisasi #'.$data_user['id'],$html_bvpr,'',$btn_bvpr[$data_user['id']]);
                                        
                                        $no++;
                                    }
                                }
                                else{
                                ?>
                                        <tr>
                                            <td colspan="9">- empty -</td>
                                        </tr>
                                <?php
                                }
                                ?>
                                       
                                        
                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>

<script>
$(document).ready(function(){
    <?php
    $sql_user = "SELECT * FROM pendaftar where hapus!='1'";
    $result_user = mysqli_query($koneksi, $sql_user);
    if(mysqli_num_rows($result_user)> 0) {
        while($data_user = mysqli_fetch_array($result_user)){
    ?>
    $('.iver<?=$data_user['nbm']?>').on('click', function () {
        if (confirm("\nApakah Anda yakin akan memverifikasi data ini?\n") == true){
                    var form_data = new FormData();
                    var id=$(".iver<?=$data_user['nbm']?>").attr("id");
                    form_data.append('keterangan', $('.keterangan_'+id).val());
                    form_data.append('status', $('input[name=status_verifikasi_'+id+']').val());
                    form_data.append('NBM', id);
                    console.log($('.keterangan_'+id).val()+ ' - '+$('input[name=status_verifikasi_'+id+']').val()+id)
                    $.ajax({
                        url: '<?=base_url("proses_verifikasi.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            //$('.unggah').html(response); // display success response from the server
                            // alert('berhasil');
                            window.location.replace("<?=base_url('utusan-'.$_GET['id'])?>");
                        },
                        error: function (response) {
                            window.location.replace("<?=base_url('utusan-'.$_GET['id'])?>");
                        }
                    });
        }
        else{}
    });
    
    <?php 
        } 
    } 
    ?>
});
</script>
