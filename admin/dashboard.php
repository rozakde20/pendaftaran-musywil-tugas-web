<?php include('../config/auto_load.php');
include('../template/header.php');
$sql_blm_verifikasi="select count(*) as n from pendaftar a 
left join tbl_verifikasi_pendaftar b on a.id=b.id_pendaftar 
left join data_kirim c on a.user_daerah=c.daerah where a.hapus='0' and (b.id is null || b.verifikasi='2') and c.id is not null;";
$sql_verifikasi="select count(*) as n from pendaftar a 
left join tbl_verifikasi_pendaftar b on a.id=b.id_pendaftar 
left join data_kirim c on a.user_daerah=c.daerah where a.hapus='0' and b.verifikasi='1' and c.id is not null;";
$nblm_ver=_sq($sql_blm_verifikasi)->fetch_object()->n;
$nblm_ver=($nblm_ver==''?'0':$nblm_ver);


$nsdh_ver=_sq($sql_verifikasi)->fetch_object()->n;
$nsdh_ver=($nsdh_ver==''?'0':$nsdh_ver);
$ntotal=$nblm_ver+$nsdh_ver;
$pblmver=($nblm_ver/$ntotal)*100;
$psdhver=($nsdh_ver/$ntotal)*100;

?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    
                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Dashboard Admin</h1>
                    <?php
                    if(isset($_SESSION['flash_message_success'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-check-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Success</h6>
                                    <?=$_SESSION['flash_message_success'];unset($_SESSION['flash_message_success']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                    if(isset($_SESSION['flash_message_error'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-times-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Failed</h6>
                                    <?=$_SESSION['flash_message_error'];unset($_SESSION['flash_message_error']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <div class="row">
                    <div class="col-md-6">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h4 font-weight-bold text-warning text-uppercase mb-1">BELUM TERVERIFIKASI
                                            </div>
                                            <div class="h5 mt-3 font-weight-bold"><?=$nblm_ver?> orang</div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col">
                                                    <div class="progress progress-sm mr-2" style="margin-left:10px;">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: <?=$pblmver?>%" aria-valuenow="<?=$nblm_ver?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users fa-2x text-gray-300 " style="font-size: 70px;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="col-md-6">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h4 font-weight-bold text-success text-uppercase mb-1">TERVERIFIKASI
                                                </div>
                                                <div class="h5 mt-3 font-weight-bold"><?=$nsdh_ver?> orang</div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2" style="margin-left:10px;">
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: <?=$psdhver?>%" aria-valuenow="<?=$nsdh_ver?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-users fa-2x text-gray-300 " style="font-size: 70px;"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    <hr class="mt-3">

                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="card-header">DATA DAERAH KIRIM DATA 
                                <a class="btn btn-sm bg-teal" style="color:white;" data-toggle="modal" data-target="#daerah">Lihat Data Daerah</a>
                                </div>
                                <?php
                                $html='';
                                    $sql = "SELECT * FROM `tbl_daerah`";
                                    $result = mysqli_query($koneksi, $sql);
                                    if(mysqli_num_rows($result)> 0) {
                                      $html.='<table class="table table-bordered table-hover">
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Daerah</th>
                                                <th>Kode Daerah</th>
                                            </tr>';
                                      while($obj = mysqli_fetch_array($result)){
                                        $html.='<tr>
                                                <td>'.$obj['no_daerah'].'</td>
                                                <td>'.$obj['nama_daerah'].'</td>
                                                <td>'.$obj['kode_daerah'].'</td>
                                            </tr>';
                                      }
                                      $html.='</table>';
                                      $result -> free_result();
                                    }  
                                echo modal('daerah','Data Daerah Pemuda Muhammadiyah Jawa Tengah',$html);
                                ?>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover datatable-table">
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Daerah</th>
                                                <th>Jumlah Utusan</th>
                                                <th>Tanggal Kirim</th>
                                                <th>Pengirim</th>
                                                <th>Progres Verifikasi</th>
                                                <th>Total Transfer</th>
                                                <th>Aksi</th>
                                            </tr>
                                            <?php
                                            $sql_user = "SELECT * FROM data_kirim where status_kirim='1' order by id";
                                                $result_user = mysqli_query($koneksi, $sql_user);
                                                if(mysqli_num_rows($result_user)>0) {
                                                    $no=1;
                                                    while($data_user = mysqli_fetch_array($result_user)){
                                                        //detail orang
                                                        $sp="SELECT kode_pimpinan,count(*) as n FROM `pendaftar` where user_daerah='".$data_user['daerah']."' and  hapus='0' GROUP BY kode_pimpinan;";
                                                        $rp=  mysqli_query($koneksi, $sp);
                                                        $h='';
                                                        $swo=0;$swp=0;
                                                        if(mysqli_num_rows($rp)>0) {
                                                            while($drp = mysqli_fetch_array($rp)){
                                                                $h.=rtrim('<br/>'.desc_kodepim($drp['kode_pimpinan']).' '.$drp['n'].' orang,', ',');
                                                                if($drp['kode_pimpinan']=='1'){$swo+=0;}
                                                                if($drp['kode_pimpinan']=='2'){$swo+=400000;$swp+=$drp['n']*400000;}
                                                                if($drp['kode_pimpinan']=='3'){$swo+=100000;$swp+=$drp['n']*400000;}
                                                                
                                                            }
                                                        }
                                                        $swc=((_jumlah_pcpm($data_user['daerah'])-1)*100000);
                                                        $swo=$swo+$swc;
                                                        $sw=($swo+$swp);
                                                        //end detail orang
                                                        $selesai[$data_user['daerah']]=_row('daerah_selesai_verifikasi','id,daerah','id','daerah="'.$data_user['daerah'].'" and status_selesai="1"');
                                                        echo "<tr>
                                                        <td>".$no.".</td>
                                                        <td>".nama_daerah($data_user['daerah'])." (".$data_user['daerah'].")</td>
                                                        <td><b>".count_utusan($data_user['daerah'])." Orang:</b>".$h."</td>
                                                        <td>".datetime_to_tanggal_custom2($data_user['tgl_kirim'])."</td>
                                                        <td>".user_nama($data_user['user_id'])."</td>
                                                        <td>".count_utusan_verifikasi($data_user['daerah'])."/".count_utusan($data_user['daerah'])."</td>
                                                        <td>0/".rupiah($sw)."<br/>SWO ".rupiah($swo)."<br/>SWP ".rupiah($swp)."</td>
                                                        <td>".
                                                        ($selesai[$data_user['daerah']]>0?"<span class='btn btn-sm btn-outline-success'>Selesai Verifikasi Daerah</span><br/>
                                                            <a href='verifikasi-berkas-"._smgenc($data_user['id'])."'  class='btn btn-primary btn-sm' style=\"margin-top:5px;\">Verifikasi Berkas</a> ":"
                                                            <a href='utusan-"._smgenc($data_user['id'])."' class='btn btn-primary btn-sm'>Detail</a> 
                                                            <a data-toggle=\"modal\" data-target=\"#kembalikan".$data_user['id']."\" 
                                                            class='btn btn-danger btn-sm' title='dikembalikan ke daerah'>Kembalikan</a>
                                                            <a data-toggle=\"modal\" data-target=\"#selesaiverifikasi".$data_user['id']."\" 
                                                            class='btn btn-pink btn-sm' title='selesai verifikasi & kirim notifikasi pembayaran'>Finish & kirim notif bayar</a>")
                                                        ."
                                                        </td>
                                                        </tr>";
                                                        
                                                        //Modal kembalikan
                                                        $html_kembalikan="Apakah Anda Yakin akan mengembalikan ke Daerah?<br/>
                                                        ";
                                                        echo modal('kembalikan'.$data_user['id'],'Konfimasi #'.$data_user['id'],$html_kembalikan,'','<a class="btn btn-danger" href="kembalikan-'._smgenc($data_user['id']).'">Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>');
                                                        
                                                        //Modal selesaiverifikasi
                                                        $html_selesaiverifikasi="Apakah Anda telah menyelesaikan semua verifikasi?<br/>
                                                        <font color=\"red\" size=\"2px\">(setelah mengklik tombol <b>Ya</b> maka anda telah mengakhiri proses verifikasi dan mengirim notifikasi ke daerah untuk melakukan pembayaran.)</font>
                                                        ";
                                                        
                                                        $btn[$data_user['id']]='<a class="btn btn-danger" href="selesai-verifikasi-'._smgenc($data_user['daerah'].'#'.$sw.'#1').'">Ya</a> <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>';
                                                        echo modal('selesaiverifikasi'.$data_user['id'],'Konfimasi #'.$data_user['id'],$html_selesaiverifikasi,'',$btn[$data_user['id']]);
                                                        $no++;
                                                    }//endwhile
                                                }
                                                 else{
                                                ?>
                                                        <tr>
                                                            <td colspan="7">- empty -</td>
                                                        </tr>
                                                <?php
                                                }
                                                ?>
                                            
                                        </table><?=is_bool($result_user)?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>
<script>
$(document).ready(function(){
    $('.datatable-table').DataTable();
});
</script>