<?php include('../config/auto_load.php') ?>
<?php include('../template/header.php') ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <!--<h1 class="h3 mb-4 text-gray-800">Data Utusan</h1>-->
                    
                    <?php
                    if(isset($_SESSION['flash_message_success'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-check-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Success</h6>
                                    <?=$_SESSION['flash_message_success'];unset($_SESSION['flash_message_success']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                    if(isset($_SESSION['flash_message_error'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-icon" role="alert">
                                <!--<button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>-->
                                <div class="alert-icon-aside">
                                    <i class="fas fa-times-circle"></i>
                                </div>
                                <div class="alert-icon-content">
                                    <h6 class="alert-heading">Failed</h6>
                                    <?=$_SESSION['flash_message_error'];unset($_SESSION['flash_message_error']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--<hr class="mt-3">-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                    <h1 class="h3 mb-0 text-gray-800">Data Utusan <?=nama_daerah(id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))).' '.id_data_kirim_to_kode_daerah(_smgdec($_GET['id']))?></h1>
                                    <div><a href="dashboard" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Back</a></div>
                                    </div>
                                </div>
                                <div class="card-header table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td>No</td>
                                            <td>Nama</td>
                                            <td>NBM</td>
                                            <td>Jabatan</td>
                                            <td>Utusan</td>
                                            <td>Daerah</td>
                                            <td>Cabang</td>
                                            <td>HP</td>
                                            <td>Email</td>
                                            <td>Status</td>
                                            <td>Foto</td>
                                            <td>KTP</td>
                                            <td>NBM</td>
                                            <td>option</td>
                                        </tr>
                                        <?php
                                        $sql_user = "SELECT * FROM pendaftar where hapus!='1'";
                                    $result_user = mysqli_query($koneksi, $sql_user);
                                    if(mysqli_num_rows($result_user)> 0) {
                                        $no=1;
                                        while($data_user = mysqli_fetch_array($result_user)){
                                            $verifikasi=_row('tbl_verifikasi_pendaftar','id_pendaftar,verifikasi','verifikasi','id_pendaftar="'.nbm_to_id($data_user['nbm']).'"');
                                            $keterangan=_row('tbl_verifikasi_pendaftar','id_pendaftar,keterangan','keterangan','id_pendaftar="'.nbm_to_id($data_user['nbm']).'"');
                                            echo "<tr>
                                            <td>".$no.".</td>
                                            <td>".$data_user['nama']."</td>
                                            <td>".$data_user['nbm']."</td>
                                            <td>".$data_user['jabatan']."</td>
                                            <td>".$data_user['pimpinan']." (".$data_user['kode_pimpinan'].")</td>
                                            <td>".nama_daerah($data_user['daerah'])."</td>
                                            <td>".nama_cabang($data_user['cabang'])."</td>
                                            <td>".$data_user['nomor']."</td>
                                            <td>".$data_user['email']."</td>
                                            <td>".
                                            (($verifikasi=='1')?"<span class='btn btn-sm btn-outline-success'>Terverifikasi</span>":(($verifikasi=='2')?"<span class='btn btn-sm btn-outline-danger'>ditolak</span>":"<span class='btn btn-sm btn-outline-warning'>waiting</span>"))."
                                            </td>
                                            <td><a href='".$data_user['url_foto']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#foto".$data_user['nbm']."\">Lihat Foto</a></td>
                                            <td><a href='".$data_user['url_ktp']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#ktp".$data_user['nbm']."\">Lihat KTP</a></td>
                                            <td><a href='".$data_user['url_nbm']."' target=\"_blank\" data-toggle=\"modal\" data-target=\"#nbm".$data_user['nbm']."\">Lihat NBM</a></td>
                                            <td>".
                                            (($verifikasi=='1')?"<span class='btn btn-sm btn-outline-success'>Terverifikasi</span>":"
                                            <a href=\"".home_base_url()."admin/verifikasi-"._smgenc($data_user['nbm'])."\"  target=\"_blank\" data-toggle=\"modal\" data-target=\"#verifikasi".$data_user['nbm']."\" class=\"btn btn-sm btn-orange\">Verifikasi</a>")
                                            ."</td>
                                            </tr>";// <td><a href=\"".home_base_url()."peserta/detail-"._smgenc($data_user['nbm'])."\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-user\"></i> Detail</a></td>
                                            //Modal Foto
                                            $html_foto="<image src='".$data_user['url_foto']."'><br/>".$data_user['nama'];
                                            echo modal('foto'.$data_user['nbm'],'Foto Peserta NBM.'.$data_user['nbm'],$html_foto);
                                            
                                            //Modal KTP
                                            $html_ktp="<image src='".$data_user['url_ktp']."'><br/>".$data_user['nama'];
                                            echo modal('ktp'.$data_user['nbm'],'Foto KTP NBM.'.$data_user['nbm'],$html_ktp);
                                            
                                            //Modal KTP
                                            $html_nbm="<image src='".$data_user['url_nbm']."'><br/>".$data_user['nama'];
                                            echo modal('nbm'.$data_user['nbm'],'Foto NBM.'.$data_user['nbm'],$html_nbm);
                                            
                                            //Modal Verifikasi
                                            $checked_ver1=(($verifikasi=='1')?'checked':'');
                                            $checked_ver2=(($verifikasi=='2')?'checked':'');
                                            $html_ver='
                                            <form class="user">
                                                <div class="form-group">
                                                    <label class="small mb-1">Status Verifikasi</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="status_verifikasi_'.$data_user['nbm'].'" type="radio" value="1" '.$checked_ver1.'>
                                                        <label class="form-check-label">setujui</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="status_verifikasi_'.$data_user['nbm'].'" type="radio" value="2"  '.$checked_ver2.'>
                                                        <label class="form-check-label">ditolak</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small mb-1">Keterangan Verifikasi</label>
                                                    <input type="text" class="form-control form-control-user keterangan_'.$data_user['nbm'].'" value="'.$keterangan.'" placeholder="Keterangan Verifikasi">
                                                </div>
                                                <a class="btn btn-primary btn-user btn-block iver'.$data_user['nbm'].'" id="'.$data_user['nbm'].'">
                                                    Verifikasi
                                                </a>
                                                
                                            </form>
                                            <div class="unggah"></div>
                                            ';
                                            echo modal('verifikasi'.$data_user['nbm'],'Halaman Verifikasi NBM.'.$data_user['nbm'],$html_ver);
                                            
                                            $no++;
                                        }
                                    }
                                    else{
                                    ?>
                                            <tr>
                                                <td colspan="9">- empty -</td>
                                            </tr>
                                    <?php
                                    }
                                    ?>
                                        
                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
                <!-- /.container-fluid -->

<?php include('../template/footer.php') ?>

<script>
$(document).ready(function(){
    <?php
    $sql_user = "SELECT * FROM pendaftar where hapus!='1'";
    $result_user = mysqli_query($koneksi, $sql_user);
    if(mysqli_num_rows($result_user)> 0) {
        while($data_user = mysqli_fetch_array($result_user)){
    ?>
    $('.iver<?=$data_user['nbm']?>').on('click', function () {
        if (confirm("\nApakah Anda yakin akan memverifikasi data ini?\n") == true){
                    var form_data = new FormData();
                    var id=$(".iver<?=$data_user['nbm']?>").attr("id");
                    form_data.append('keterangan', $('.keterangan_'+id).val());
                    form_data.append('status', $('input[name=status_verifikasi_'+id+']').val());
                    form_data.append('NBM', id);
                    console.log($('.keterangan_'+id).val()+ ' - '+$('input[name=status_verifikasi_'+id+']').val()+id)
                    $.ajax({
                        url: '<?=base_url("proses_verifikasi.php")?>', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            //$('.unggah').html(response); // display success response from the server
                            // alert('berhasil');
                            window.location.replace("<?=base_url('utusan-'.$_GET['id'])?>");
                        },
                        error: function (response) {
                            window.location.replace("<?=base_url('utusan-'.$_GET['id'])?>");
                        }
                    });
        }
        else{}
    });
    
    <?php 
        } 
    } 
    ?>
});
</script>
